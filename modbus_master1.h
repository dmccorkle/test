#ifndef MODBUS_MASTER1_H
#define	MODBUS_MASTER1_H

void    modbus_master1_execute(void);
void    modbus_master1_set_state(modbus_state_t state);

#endif	/* MODBUS_MASTER1_H */