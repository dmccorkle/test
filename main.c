#include    <stdio.h>
#include    "system/system.h"

#include    "timers.h"
#include    "register.h"
#include    "modbus_master1.h"
#include    "modbus_master2.h"
#include    "sensors.h"



void    main(void)
{
    
    uint16_t time_now = timers_now_second();//0x3FFF;
    uint16_t time_wait = 9;//0x3FFF;
    
    system_initialize();
    register_load_hard_coded_values();
    
    
    while(1)
    {
        
        /* Code Goes Here */
        timers_execute(); 
        
        if(timers_is_second_timer_expired(time_now, time_wait)){
            
            time_now = timers_now_second();
            time_wait = 9;
            
            _modbus_master2_create_read(DANFOSS_ADDRESS, SENSOR_READ_I , DANFOSS_REG_LFL, 0x0004);
                        
        }       
        
                      
    }
}