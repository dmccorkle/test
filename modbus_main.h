#ifndef MODBUS_MAIN_H
#define	MODBUS_MAIN_H

void    modbus_main_execute(void);
void modbus_main_set_state(modbus_state_t state);

#endif	/* MODBUS_MAIN_H */

