#include    "system/system.h"

#include    "modbus_master2.h"
#include    "register.h"


crc_t           modbus_master2_crc;

#define     MASTER2_ADDRESS g_5102.lsb_u8



void    _modbus_master2_idle(void)
{
    tmr3_set_time(DELAY_20_MS);
    tmr3_reset_timer();
}

void    _modbus_master2_set_receive(void)
{
    uart3_set_bytes_received(0);
    UART3_ENABLE = 0;
    uart3_set_receive_ready();
    /* TODO: COLLISION DETECTION? */
    uart3_reset_crc();       
}

void    _modbus_master2_start_transmission(void)
{
    uart3_disable_interrupts();
    tmr3_clear_interrupt();
    uart3_reset_crc();
}

void    _modbus_master2_set_transmit(void)
{
    
    tmr3_set_time(DELAY_2_13_MS);
    tmr3_reset_timer();
}

bool    _modbus_master2_check_function(void)
{
    return ((MODBUS_WRITE_REGISTER != uart3_get_input_buffer_byte(MODBUS_PACKET_COMMAND)) &&
            (MODBUS_READ_REGISTER != uart3_get_input_buffer_byte(MODBUS_PACKET_COMMAND)));
}

bool    _modbus_master2_check_register_address(void)
{
    return ((0x51 != uart3_get_input_buffer_byte(MODBUS_PACKET_REGISTER_HIGH) && 
            MODBUS_READ_REGISTER == uart3_get_input_buffer_byte(MODBUS_PACKET_COMMAND)) ||
            (0x50 != uart3_get_input_buffer_byte(MODBUS_PACKET_REGISTER_HIGH) &&
            MODBUS_WRITE_REGISTER == uart3_get_input_buffer_byte(MODBUS_PACKET_COMMAND)));
}

bool    _modbus_master2_check_message_length(void)
{
    return ((uart3_get_input_buffer_byte(MODBUS_PACKET_NUMBER_OF_BYTES) != (2 * uart3_get_input_buffer_byte(MODBUS_PACKET_NUMBER_OF_REGISTERS))) &&
            MODBUS_WRITE_REGISTER == uart3_get_input_buffer_byte(MODBUS_PACKET_COMMAND));
}

bool    _modbus_master2_illegal_message(void)
{
    /* check for supported function, address range, data value 
     * return true if anything bad after setting message exception codes */
    uint8_t modbus_exception_code = 0x00;
    if (_modbus_master2_check_function())
    {
        modbus_exception_code = 0x01;
    }
    else if (_modbus_master2_check_register_address())
    {
        modbus_exception_code = 0x02;
    }
    else if (_modbus_master2_check_message_length())
    {
        modbus_exception_code = 0x03;
    }
    
    if (0x00 != modbus_exception_code)
    {
       uart3_set_transmit_buffer_byte(MASTER2_ADDRESS, MODBUS_PACKET_ADDRESS);
       uart3_set_transmit_buffer_byte(uart3_get_input_buffer_byte(MODBUS_PACKET_COMMAND) + 0x80, MODBUS_PACKET_COMMAND);
       uart3_set_transmit_buffer_byte(modbus_exception_code,2);
       uart3_set_bytes_to_transmit(3);
       return true;
    }
    return false;

}

void    _modbus_master2_no_state(void)
{
    if (modbus_master2_crc.crc_u16 > 0)
    {
        _modbus_master2_idle();
        _modbus_master2_set_receive();
    }
    else if (MASTER2_ADDRESS == uart3_get_input_buffer_byte(MODBUS_PACKET_ADDRESS))
    {
        
        _modbus_master2_set_transmit();
        _modbus_master2_set_receive();
       
    }
    else
    {
        _modbus_master2_idle();
        _modbus_master2_set_receive();
    }
}



bool _modbus_master2_create_write(uint8_t id, uint8_t func, uint16_t reg, uint8_t num_of_regs, uint16_t *arry )
{
    uint8_t count = 0;
    uint8_t reg_l, reg_h;
    
    
    uart3_set_transmit_buffer_byte(id, count);
    ++count;
    
    uart3_set_transmit_buffer_byte(func, count);
    ++count;
    
    reg_h = reg >> 8;
    reg_l = reg &0x00FF;
    
    uart3_set_transmit_buffer_byte(reg_h, count);
    ++count;
    
    uart3_set_transmit_buffer_byte(reg_l, count);
    ++count;
    
    uart3_set_transmit_buffer_byte(num_of_regs, count);
    ++count;
    
    for(char i = 0; i < num_of_regs; i++){
        
        reg_h = arry[i] >> 8;
        reg_l = arry[i] & 0x00FF;
        
        uart3_set_transmit_buffer_byte(reg_h, count);
        ++count;
        uart3_set_transmit_buffer_byte(reg_l, count);
        ++count;
    }
    
    uart3_set_bytes_to_transmit(count);
    
    //mm2_flag = true;
    uart3_send_byte();
}

bool _modbus_master2_create_read(uint8_t id, uint8_t func, uint16_t reg, uint16_t num_of_regs)
{
    uint8_t count = 0;
    uint8_t reg_h, reg_l;
    
    uart3_set_transmit_buffer_byte(id, count);
    ++count;
    
    uart3_set_transmit_buffer_byte(func, count);
    ++count;
    
    reg_h = reg >> 8;
    reg_l = reg & 0x00ff;
    
    uart3_set_transmit_buffer_byte(reg_h, count);
    ++count;
    
    uart3_set_transmit_buffer_byte(reg_l, count);
    ++count;
    
    reg_h = num_of_regs >> 8;
    reg_l = num_of_regs & 0x00FF;
            
    uart3_set_transmit_buffer_byte(reg_h, count);
    ++count;
    
    uart3_set_transmit_buffer_byte(reg_l, count);
    ++count;
    
    uart3_set_bytes_to_transmit(count);
    
    //mm2_flag = true;
    uart3_send_byte();    
}

