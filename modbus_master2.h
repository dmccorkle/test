#ifndef MODBUS_MASTER2_H
#define	MODBUS_MASTER2_H


extern bool     _modbus_master2_create_write(uint8_t id, uint8_t func, uint16_t reg, uint8_t num_of_regs, uint16_t *arry );
extern bool     _modbus_master2_create_read(uint8_t id, uint8_t func, uint16_t reg, uint16_t num_of_regs);


#endif	/* MODBUS_MASTER2_H */