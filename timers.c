#include    <stdint.h>
#include    <stdbool.h>
#include    "system/line_sync.h"

#include    "timers.h"

#define     TIMER_MAX       0x7FFF
#define     DELTA_MAX       0x3FFF
#define     TENTHS_PER_SECOND   10
#define     SECONDS_PER_MINUTE  60
#define     MINUTES_PER_HOUR    60
#define     HOURS_PER_DAY       24

static volatile uint16_t    system_timer_tenth_second_u16 = 0;
static volatile uint16_t    system_timer_second_u16 = 0;
static volatile uint16_t    system_timer_minute_u16 = 0;
static volatile uint16_t    system_timer_hour_u16 = 0;
static volatile uint16_t    system_timer_day_u16 = 0;

static volatile uint16_t    tenth_update_u16 = 0;
static volatile uint16_t    second_update_u16 = 0;
static volatile uint16_t    minute_update_u16 = 0;
static volatile uint16_t    hour_update_u16 = 0;


void    _timers_increment_system_timer( volatile uint16_t * p_system_timer)
{
    ++*p_system_timer;
    *p_system_timer &= TIMER_MAX;
}

uint16_t    _timers_get_delta(uint16_t time_start, uint16_t time_end)
{
    uint16_t    return_delta_time_u16 = 0;
    if ((TIMER_MAX >= time_start) && (TIMER_MAX >= time_end))
    {
        if (time_start > time_end)
        {
            return_delta_time_u16 = (((TIMER_MAX - time_start) + time_end + 1) & TIMER_MAX);
        }
        else
        {
            return_delta_time_u16 = time_end - time_start;
        }
    }
    return (DELTA_MAX < return_delta_time_u16 ? 0 : return_delta_time_u16);
}

bool	timers_is_tenth_second_timer_expired(uint16_t time_in, uint16_t period)
{
	return (_timers_get_delta(time_in, system_timer_tenth_second_u16) >= period);
}

bool	timers_is_second_timer_expired(uint16_t time_in, uint16_t period)
{
	return (_timers_get_delta(time_in, system_timer_second_u16) >= period);
}

bool	timers_is_minute_timer_expired(uint16_t time_in, uint16_t period)
{
	return (_timers_get_delta(time_in, system_timer_minute_u16) >= period);
}

bool	timers_is_hour_timer_expired(uint16_t time_in, uint16_t period)
{
	return (_timers_get_delta(time_in, system_timer_hour_u16) >= period);
}

bool	timers_is_day_timer_expired(uint16_t time_in, uint16_t period)
{
	return (_timers_get_delta(time_in, system_timer_day_u16) >= period);
}

void    _timers_update_all_timers(void)
{
    if (timers_is_tenth_second_timer_expired(tenth_update_u16, TENTHS_PER_SECOND))
    {
        _timers_increment_system_timer(&system_timer_second_u16);
        tenth_update_u16 += TENTHS_PER_SECOND;
        tenth_update_u16 &= TIMER_MAX;
        if (timers_is_second_timer_expired(second_update_u16, SECONDS_PER_MINUTE))
        {
            _timers_increment_system_timer(&system_timer_minute_u16);
            second_update_u16 += SECONDS_PER_MINUTE;
            second_update_u16 &= TIMER_MAX;
            if (timers_is_minute_timer_expired(minute_update_u16, MINUTES_PER_HOUR))
            {
                _timers_increment_system_timer(&system_timer_hour_u16);
                minute_update_u16 += MINUTES_PER_HOUR;
                minute_update_u16 &= TIMER_MAX;
                if (timers_is_hour_timer_expired(hour_update_u16, HOURS_PER_DAY))
                {
                    _timers_increment_system_timer(&system_timer_day_u16);
                    hour_update_u16 += HOURS_PER_DAY;
                    hour_update_u16 &= TIMER_MAX;
                }
            }
        }
     }
}

void    timers_execute(void)
{
    if (line_sync_tenth_second_check())
    {
        line_sync_clear_tenth_second_flag();
        _timers_increment_system_timer(&system_timer_tenth_second_u16);
        _timers_update_all_timers();
    }
}

uint16_t	timers_now_tenth(void)
{
	return system_timer_tenth_second_u16;
}

uint16_t	timers_now_second(void)
{
	return system_timer_second_u16;
}

uint16_t	timers_now_minute(void)
{
	return system_timer_minute_u16;
}

uint16_t	timers_now_hour(void)
{
	return system_timer_hour_u16;
}

uint16_t	timers_now_day(void)
{
	return system_timer_day_u16;
}