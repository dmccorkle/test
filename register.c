#include    <stdint.h>

#include    "system/ccg_typedef.h"
#include    "register.h"

modbus_register_t   unused_read_address;
modbus_register_t   unused_write_address;

// Read Register
modbus_register_t   g_5100;
modbus_register_t   g_5101;
modbus_register_t   g_5102;
modbus_register_t   g_5105;
modbus_register_t   g_5106;
modbus_register_t   g_5107;
modbus_register_t   g_5108;
modbus_register_t   g_510B;
modbus_register_t   g_510C;
modbus_register_t   g_510D;     //sensor fault Code
modbus_register_t   g_510F;
modbus_register_t   g_5110;     //sensor 1 LFL%
modbus_register_t   g_5111;     //sensor 2 LFL%

modbus_register_t   g_5115;     // \ sensor 1 serial number    
modbus_register_t   g_5116;     // /

modbus_register_t   g_5117;     // \ sensor 2 serial number
modbus_register_t   g_5118;     // /

modbus_register_t   g_51FD;     //  |
modbus_register_t   g_51FE;     //  ----climate master test/initialization flags 
modbus_register_t   g_51FF;     //  |

//Write Register
modbus_register_t   g_5000;     // Control Reset Value
modbus_register_t   g_5002;     // sensor address
modbus_register_t   g_5005;     // option configuration
modbus_register_t   g_5006;     // Alarm Threshold
modbus_register_t   g_5008;     // Control Flags/Status

modbus_register_t   g_50FD;     //  |
modbus_register_t   g_50FE;     //  ----climate master test/initialization flags 

static modbus_register_t *const read_registers[0x100] = 
{
    &unused_read_address,                  /*  0x5100                                     */
    &g_5101,                               /*  0x5101                                     */
    &g_5102,                               /*  0x5102                                     */
    &unused_read_address,                  /*  0x5103                                     */
    &unused_read_address,                  /*  0x5104                                     */
    &g_5105,                               /*  0x5105                                     */
    &g_5106,                               /*  0x5106                                     */
    &g_5107,                               /*  0x5107                                     */
    &g_5108,                               /*  0x5108                                     */
    &unused_read_address,                  /*  0x5109                                     */
    &unused_read_address,                  /*  0x510A                                     */
    &g_510B,                               /*  0x510B                                     */
    &g_510C,                               /*  0x510C                                     */
    &g_510D,                               /*  0x510D                                     */
    &unused_read_address,                  /*  0x510E                                     */
    &g_510F,                               /*  0x510F                                     */
    &g_5110,                               /*  0x5110                                     */
    &g_5111,                               /*  0x5111                                     */
    &unused_read_address,                  /*  0x5112                                     */
    &unused_read_address,                  /*  0x5113                                     */
    &unused_read_address,                  /*  0x5114                                     */
    &g_5115,                               /*  0x5115                                     */
    &g_5116,                               /*  0x5116                                     */
    &unused_read_address,                  /*  0x5117                                     */
    &unused_read_address,                  /*  0x5118                                     */
    &unused_read_address,                  /*  0x5119                                     */
    &unused_read_address,                  /*  0x511A                                     */
    &unused_read_address,                  /*  0x511B                                     */
    &unused_read_address,                  /*  0x511C                                     */
    &unused_read_address,                  /*  0x511D                                     */
    &unused_read_address,                  /*  0x511E                                     */
    &unused_read_address,                  /*  0x511F                                     */
    &unused_read_address,                  /*  0x5120                                     */
    &unused_read_address,                  /*  0x5121                                     */
    &unused_read_address,                  /*  0x5122                                     */
    &unused_read_address,                  /*  0x5123                                     */
    &unused_read_address,                  /*  0x5124                                     */
    &unused_read_address,                  /*  0x5125                                     */
    &unused_read_address,                  /*  0x5126                                     */
    &unused_read_address,                  /*  0x5127                                     */
    &unused_read_address,                  /*  0x5128                                     */
    &unused_read_address,                  /*  0x5129                                     */
    &unused_read_address,                  /*  0x512A                                     */
    &unused_read_address,                  /*  0x512B                                     */
    &unused_read_address,                  /*  0x512C                                     */
    &unused_read_address,                  /*  0x512D                                     */
    &unused_read_address,                  /*  0x512E                                     */
    &unused_read_address,                  /*  0x512F                                     */
    &unused_read_address,                  /*  0x5130                                     */
    &unused_read_address,                  /*  0x5131                                     */
    &unused_read_address,                  /*  0x5132                                     */
    &unused_read_address,                  /*  0x5133                                     */
    &unused_read_address,                  /*  0x5134                                     */
    &unused_read_address,                  /*  0x5135                                     */
    &unused_read_address,                  /*  0x5136                                     */
    &unused_read_address,                  /*  0x5137                                     */
    &unused_read_address,                  /*  0x5138                                     */
    &unused_read_address,                  /*  0x5139                                     */
    &unused_read_address,                  /*  0x513A                                     */
    &unused_read_address,                  /*  0x513B                                     */
    &unused_read_address,                  /*  0x513C                                     */
    &unused_read_address,                  /*  0x513D                                     */
    &unused_read_address,                  /*  0x513E                                     */
    &unused_read_address,                  /*  0x513F                                     */
    &unused_read_address,                  /*  0x5140                                     */
    &unused_read_address,                  /*  0x5141                                     */
    &unused_read_address,                  /*  0x5142                                     */
    &unused_read_address,                  /*  0x5143                                     */
    &unused_read_address,                  /*  0x5144                                     */
    &unused_read_address,                  /*  0x5145                                     */
    &unused_read_address,                  /*  0x5146                                     */
    &unused_read_address,                  /*  0x5147                                     */
    &unused_read_address,                  /*  0x5148                                     */
    &unused_read_address,                  /*  0x5149                                     */
    &unused_read_address,                  /*  0x514A                                     */
    &unused_read_address,                  /*  0x514B                                     */
    &unused_read_address,                  /*  0x514C                                     */
    &unused_read_address,                  /*  0x514D                                     */
    &unused_read_address,                  /*  0x514E                                     */
    &unused_read_address,                  /*  0x514F                                     */
    &unused_read_address,                  /*  0x5150                                     */
    &unused_read_address,                  /*  0x5151                                     */
    &unused_read_address,                  /*  0x5152                                     */
    &unused_read_address,                  /*  0x5153                                     */
    &unused_read_address,                  /*  0x5154                                     */
    &unused_read_address,                  /*  0x5155                                     */
    &unused_read_address,                  /*  0x5156                                     */
    &unused_read_address,                  /*  0x5157                                     */
    &unused_read_address,                  /*  0x5158                                     */
    &unused_read_address,                  /*  0x5159                                     */
    &unused_read_address,                  /*  0x515A                                     */
    &unused_read_address,                  /*  0x515B                                     */
    &unused_read_address,                  /*  0x515C                                     */
    &unused_read_address,                  /*  0x515D                                     */
    &unused_read_address,                  /*  0x515E                                     */
    &unused_read_address,                  /*  0x515F                                     */
    &unused_read_address,                  /*  0x5160                                     */
    &unused_read_address,                  /*  0x5161                                     */
    &unused_read_address,                  /*  0x5162                                     */
    &unused_read_address,                  /*  0x5163                                     */
    &unused_read_address,                  /*  0x5164                                     */
    &unused_read_address,                  /*  0x5165                                     */
    &unused_read_address,                  /*  0x5166                                     */
    &unused_read_address,                  /*  0x5167                                     */
    &unused_read_address,                  /*  0x5168                                     */
    &unused_read_address,                  /*  0x5169                                     */
    &unused_read_address,                  /*  0x516A                                     */
    &unused_read_address,                  /*  0x516B                                     */
    &unused_read_address,                  /*  0x516C                                     */
    &unused_read_address,                  /*  0x516D                                     */
    &unused_read_address,                  /*  0x516E                                     */
    &unused_read_address,                  /*  0x516F                                     */
    &unused_read_address,                  /*  0x5170                                     */
    &unused_read_address,                  /*  0x5171                                     */
    &unused_read_address,                  /*  0x5172                                     */
    &unused_read_address,                  /*  0x5173                                     */
    &unused_read_address,                  /*  0x5174                                     */
    &unused_read_address,                  /*  0x5175                                     */
    &unused_read_address,                  /*  0x5176                                     */
    &unused_read_address,                  /*  0x5177                                     */
    &unused_read_address,                  /*  0x5178                                     */
    &unused_read_address,                  /*  0x5179                                     */
    &unused_read_address,                  /*  0x517A                                     */
    &unused_read_address,                  /*  0x517B                                     */
    &unused_read_address,                  /*  0x517C                                     */
    &unused_read_address,                  /*  0x517D                                     */
    &unused_read_address,                  /*  0x517E                                     */
    &unused_read_address,                  /*  0x517F                                     */
    &unused_read_address,                  /*  0x5180                                     */
    &unused_read_address,                  /*  0x5181                                     */
    &unused_read_address,                  /*  0x5182                                     */
    &unused_read_address,                  /*  0x5183                                     */
    &unused_read_address,                  /*  0x5184                                     */
    &unused_read_address,                  /*  0x5185                                     */
    &unused_read_address,                  /*  0x5186                                     */
    &unused_read_address,                  /*  0x5187                                     */
    &unused_read_address,                  /*  0x5188                                     */
    &unused_read_address,                  /*  0x5189                                     */
    &unused_read_address,                  /*  0x518A                                     */
    &unused_read_address,                  /*  0x518B                                     */
    &unused_read_address,                  /*  0x518C                                     */
    &unused_read_address,                  /*  0x518D                                     */
    &unused_read_address,                  /*  0x518E                                     */
    &unused_read_address,                  /*  0x518F                                     */
    &unused_read_address,                  /*  0x5190                                     */
    &unused_read_address,                  /*  0x5191                                     */
    &unused_read_address,                  /*  0x5192                                     */
    &unused_read_address,                  /*  0x5193                                     */
    &unused_read_address,                  /*  0x5194                                     */
    &unused_read_address,                  /*  0x5195                                     */
    &unused_read_address,                  /*  0x5196                                     */
    &unused_read_address,                  /*  0x5197                                     */
    &unused_read_address,                  /*  0x5198                                     */
    &unused_read_address,                  /*  0x5199                                     */
    &unused_read_address,                  /*  0x519A                                     */
    &unused_read_address,                  /*  0x519B                                     */
    &unused_read_address,                  /*  0x519C                                     */
    &unused_read_address,                  /*  0x519D                                     */
    &unused_read_address,                  /*  0x519E                                     */
    &unused_read_address,                  /*  0x519F                                     */
    &unused_read_address,                  /*  0x51A0                                     */
    &unused_read_address,                  /*  0x51A1                                     */
    &unused_read_address,                  /*  0x51A2                                     */
    &unused_read_address,                  /*  0x51A3                                     */
    &unused_read_address,                  /*  0x51A4                                     */
    &unused_read_address,                  /*  0x51A5                                     */
    &unused_read_address,                  /*  0x51A6                                     */
    &unused_read_address,                  /*  0x51A7                                     */
    &unused_read_address,                  /*  0x51A8                                     */
    &unused_read_address,                  /*  0x51A9                                     */
    &unused_read_address,                  /*  0x51AA                                     */
    &unused_read_address,                  /*  0x51AB                                     */
    &unused_read_address,                  /*  0x51AC                                     */
    &unused_read_address,                  /*  0x51AD                                     */
    &unused_read_address,                  /*  0x51AE                                     */
    &unused_read_address,                  /*  0x51AF                                     */
    &unused_read_address,                  /*  0x51B0                                     */
    &unused_read_address,                  /*  0x51B1                                     */
    &unused_read_address,                  /*  0x51B2                                     */
    &unused_read_address,                  /*  0x51B3                                     */
    &unused_read_address,                  /*  0x51B4                                     */
    &unused_read_address,                  /*  0x51B5                                     */
    &unused_read_address,                  /*  0x51B6                                     */
    &unused_read_address,                  /*  0x51B7                                     */
    &unused_read_address,                  /*  0x51B8                                     */
    &unused_read_address,                  /*  0x51B9                                     */
    &unused_read_address,                  /*  0x51BA                                     */
    &unused_read_address,                  /*  0x51BB                                     */
    &unused_read_address,                  /*  0x51BC                                     */
    &unused_read_address,                  /*  0x51BD                                     */
    &unused_read_address,                  /*  0x51BE                                     */
    &unused_read_address,                  /*  0x51BF                                     */
    &unused_read_address,                  /*  0x51C0                                     */
    &unused_read_address,                  /*  0x51C1                                     */
    &unused_read_address,                  /*  0x51C2                                     */
    &unused_read_address,                  /*  0x51C3                                     */
    &unused_read_address,                  /*  0x51C4                                     */
    &unused_read_address,                  /*  0x51C5                                     */
    &unused_read_address,                  /*  0x51C6                                     */
    &unused_read_address,                  /*  0x51C7                                     */
    &unused_read_address,                  /*  0x51C8                                     */
    &unused_read_address,                  /*  0x51C9                                     */
    &unused_read_address,                  /*  0x51CA                                     */
    &unused_read_address,                  /*  0x51CB                                     */
    &unused_read_address,                  /*  0x51CC                                     */
    &unused_read_address,                  /*  0x51CD                                     */
    &unused_read_address,                  /*  0x51CE                                     */
    &unused_read_address,                  /*  0x51CF                                     */
    &unused_read_address,                  /*  0x51D0                                     */
    &unused_read_address,                  /*  0x51D1                                     */
    &unused_read_address,                  /*  0x51D2                                     */
    &unused_read_address,                  /*  0x51D3                                     */
    &unused_read_address,                  /*  0x51D4                                     */
    &unused_read_address,                  /*  0x51D5                                     */
    &unused_read_address,                  /*  0x51D6                                     */
    &unused_read_address,                  /*  0x51D7                                     */
    &unused_read_address,                  /*  0x51D8                                     */
    &unused_read_address,                  /*  0x51D9                                     */
    &unused_read_address,                  /*  0x51DA                                     */
    &unused_read_address,                  /*  0x51DB                                     */
    &unused_read_address,                  /*  0x51DC                                     */
    &unused_read_address,                  /*  0x51DD                                     */
    &unused_read_address,                  /*  0x51DE                                     */
    &unused_read_address,                  /*  0x51DF                                     */
    &unused_read_address,                  /*  0x51E0                                     */
    &unused_read_address,                  /*  0x51E1                                     */
    &unused_read_address,                  /*  0x51E2                                     */
    &unused_read_address,                  /*  0x51E3                                     */
    &unused_read_address,                  /*  0x51E4                                     */
    &unused_read_address,                  /*  0x51E5                                     */
    &unused_read_address,                  /*  0x51E6                                     */
    &unused_read_address,                  /*  0x51E7                                     */
    &unused_read_address,                  /*  0x51E8                                     */
    &unused_read_address,                  /*  0x51E9                                     */
    &unused_read_address,                  /*  0x51EA                                     */
    &unused_read_address,                  /*  0x51EB                                     */
    &unused_read_address,                  /*  0x51EC                                     */
    &unused_read_address,                  /*  0x51ED                                     */
    &unused_read_address,                  /*  0x51EE                                     */
    &unused_read_address,                  /*  0x51EF                                     */
    &unused_read_address,                  /*  0x51F0                                     */
    &unused_read_address,                  /*  0x51F1                                     */
    &unused_read_address,                  /*  0x51F2                                     */
    &unused_read_address,                  /*  0x51F3                                     */
    &unused_read_address,                  /*  0x51F4                                     */
    &unused_read_address,                  /*  0x51F5                                     */
    &unused_read_address,                  /*  0x51F6                                     */
    &unused_read_address,                  /*  0x51F7                                     */
    &unused_read_address,                  /*  0x51F8                                     */
    &unused_read_address,                  /*  0x51F9                                     */
    &unused_read_address,                  /*  0x51FA                                     */
    &unused_read_address,                  /*  0x51FB                                     */
    &unused_read_address,                  /*  0x51FC                                     */
    &g_51FD,                               /*  0x51FD                                     */
    &g_51FE,                               /*  0x51FE                                     */
    &g_51FF,                               /*  0x51FF                                     */
};

static modbus_register_t *const write_register[0x100] =
{
    &g_5000,                                 /*  0x5000                                     */
    &unused_write_address,                  /*  0x5001                                     */
    &g_5002,                                 /*  0x5002                                     */
    &unused_write_address,                  /*  0x5003                                     */
    &unused_write_address,                  /*  0x5004                                     */
    &g_5005,                                 /*  0x5005                                     */
    &g_5006,                                 /*  0x5006                                     */
    &unused_write_address,                  /*  0x5007                                     */
    &g_5008,                                 /*  0x5008                                     */
    &unused_write_address,                  /*  0x5009                                     */
    &unused_write_address,                  /*  0x500A                                     */
    &unused_write_address,                  /*  0x500B                                     */
    &unused_write_address,                  /*  0x500C                                     */
    &unused_write_address,                  /*  0x500D                                     */
    &unused_write_address,                  /*  0x500E                                     */
    &unused_write_address,                  /*  0x500F                                     */
    &unused_write_address,                  /*  0x5010                                     */
    &unused_write_address,                  /*  0x5011                                     */
    &unused_write_address,                  /*  0x5012                                     */
    &unused_write_address,                  /*  0x5013                                     */
    &unused_write_address,                  /*  0x5014                                     */
    &unused_write_address,                  /*  0x5015                                     */
    &unused_write_address,                  /*  0x5016                                     */
    &unused_write_address,                  /*  0x5017                                     */
    &unused_write_address,                  /*  0x5018                                     */
    &unused_write_address,                  /*  0x5019                                     */
    &unused_write_address,                  /*  0x501A                                     */
    &unused_write_address,                  /*  0x501B                                     */
    &unused_write_address,                  /*  0x501C                                     */
    &unused_write_address,                  /*  0x501D                                     */
    &unused_write_address,                  /*  0x501E                                     */
    &unused_write_address,                  /*  0x501F                                     */
    &unused_write_address,                  /*  0x5020                                     */
    &unused_write_address,                  /*  0x5021                                     */
    &unused_write_address,                  /*  0x5022                                     */
    &unused_write_address,                  /*  0x5023                                     */
    &unused_write_address,                  /*  0x5024                                     */
    &unused_write_address,                  /*  0x5025                                     */
    &unused_write_address,                  /*  0x5026                                     */
    &unused_write_address,                  /*  0x5027                                     */
    &unused_write_address,                  /*  0x5028                                     */
    &unused_write_address,                  /*  0x5029                                     */
    &unused_write_address,                  /*  0x502A                                     */
    &unused_write_address,                  /*  0x502B                                     */
    &unused_write_address,                  /*  0x502C                                     */
    &unused_write_address,                  /*  0x502D                                     */
    &unused_write_address,                  /*  0x502E                                     */
    &unused_write_address,                  /*  0x502F                                     */
    &unused_write_address,                  /*  0x5030                                     */
    &unused_write_address,                  /*  0x5031                                     */
    &unused_write_address,                  /*  0x5032                                     */
    &unused_write_address,                  /*  0x5033                                     */
    &unused_write_address,                  /*  0x5034                                     */
    &unused_write_address,                  /*  0x5035                                     */
    &unused_write_address,                  /*  0x5036                                     */
    &unused_write_address,                  /*  0x5037                                     */
    &unused_write_address,                  /*  0x5038                                     */
    &unused_write_address,                  /*  0x5039                                     */
    &unused_write_address,                  /*  0x503A                                     */
    &unused_write_address,                  /*  0x503B                                     */
    &unused_write_address,                  /*  0x503C                                     */
    &unused_write_address,                  /*  0x503D                                     */
    &unused_write_address,                  /*  0x503E                                     */
    &unused_write_address,                  /*  0x503F                                     */
    &unused_write_address,                  /*  0x5040                                     */
    &unused_write_address,                  /*  0x5041                                     */
    &unused_write_address,                  /*  0x5042                                     */
    &unused_write_address,                  /*  0x5043                                     */
    &unused_write_address,                  /*  0x5044                                     */
    &unused_write_address,                  /*  0x5045                                     */
    &unused_write_address,                  /*  0x5046                                     */
    &unused_write_address,                  /*  0x5047                                     */
    &unused_write_address,                  /*  0x5048                                     */
    &unused_write_address,                  /*  0x5049                                     */
    &unused_write_address,                  /*  0x504A                                     */
    &unused_write_address,                  /*  0x504B                                     */
    &unused_write_address,                  /*  0x504C                                     */
    &unused_write_address,                  /*  0x504D                                     */
    &unused_write_address,                  /*  0x504E                                     */
    &unused_write_address,                  /*  0x504F                                     */
    &unused_write_address,                  /*  0x5050                                     */
    &unused_write_address,                  /*  0x5051                                     */
    &unused_write_address,                  /*  0x5052                                     */
    &unused_write_address,                  /*  0x5053                                     */
    &unused_write_address,                  /*  0x5054                                     */
    &unused_write_address,                  /*  0x5055                                     */
    &unused_write_address,                  /*  0x5056                                     */
    &unused_write_address,                  /*  0x5057                                     */
    &unused_write_address,                  /*  0x5058                                     */
    &unused_write_address,                  /*  0x5059                                     */
    &unused_write_address,                  /*  0x505A                                     */
    &unused_write_address,                  /*  0x505B                                     */
    &unused_write_address,                  /*  0x505C                                     */
    &unused_write_address,                  /*  0x505D                                     */
    &unused_write_address,                  /*  0x505E                                     */
    &unused_write_address,                  /*  0x505F                                     */
    &unused_write_address,                  /*  0x5060                                     */
    &unused_write_address,                  /*  0x5061                                     */
    &unused_write_address,                  /*  0x5062                                     */
    &unused_write_address,                  /*  0x5063                                     */
    &unused_write_address,                  /*  0x5064                                     */
    &unused_write_address,                  /*  0x5065                                     */
    &unused_write_address,                  /*  0x5066                                     */
    &unused_write_address,                  /*  0x5067                                     */
    &unused_write_address,                  /*  0x5068                                     */
    &unused_write_address,                  /*  0x5069                                     */
    &unused_write_address,                  /*  0x506A                                     */
    &unused_write_address,                  /*  0x506B                                     */
    &unused_write_address,                  /*  0x506C                                     */
    &unused_write_address,                  /*  0x506D                                     */
    &unused_write_address,                  /*  0x506E                                     */
    &unused_write_address,                  /*  0x506F                                     */
    &unused_write_address,                  /*  0x5070                                     */
    &unused_write_address,                  /*  0x5071                                     */
    &unused_write_address,                  /*  0x5072                                     */
    &unused_write_address,                  /*  0x5073                                     */
    &unused_write_address,                  /*  0x5074                                     */
    &unused_write_address,                  /*  0x5075                                     */
    &unused_write_address,                  /*  0x5076                                     */
    &unused_write_address,                  /*  0x5077                                     */
    &unused_write_address,                  /*  0x5078                                     */
    &unused_write_address,                  /*  0x5079                                     */
    &unused_write_address,                  /*  0x507A                                     */
    &unused_write_address,                  /*  0x507B                                     */
    &unused_write_address,                  /*  0x507C                                     */
    &unused_write_address,                  /*  0x507D                                     */
    &unused_write_address,                  /*  0x507E                                     */
    &unused_write_address,                  /*  0x507F                                     */
    &unused_write_address,                  /*  0x5080                                     */
    &unused_write_address,                  /*  0x5081                                     */
    &unused_write_address,                  /*  0x5082                                     */
    &unused_write_address,                  /*  0x5083                                     */
    &unused_write_address,                  /*  0x5084                                     */
    &unused_write_address,                  /*  0x5085                                     */
    &unused_write_address,                  /*  0x5086                                     */
    &unused_write_address,                  /*  0x5087                                     */
    &unused_write_address,                  /*  0x5088                                     */
    &unused_write_address,                  /*  0x5089                                     */
    &unused_write_address,                  /*  0x508A                                     */
    &unused_write_address,                  /*  0x508B                                     */
    &unused_write_address,                  /*  0x508C                                     */
    &unused_write_address,                  /*  0x508D                                     */
    &unused_write_address,                  /*  0x508E                                     */
    &unused_write_address,                  /*  0x508F                                     */
    &unused_write_address,                  /*  0x5090                                     */
    &unused_write_address,                  /*  0x5091                                     */
    &unused_write_address,                  /*  0x5092                                     */
    &unused_write_address,                  /*  0x5093                                     */
    &unused_write_address,                  /*  0x5094                                     */
    &unused_write_address,                  /*  0x5095                                     */
    &unused_write_address,                  /*  0x5096                                     */
    &unused_write_address,                  /*  0x5097                                     */
    &unused_write_address,                  /*  0x5098                                     */
    &unused_write_address,                  /*  0x5099                                     */
    &unused_write_address,                  /*  0x509A                                     */
    &unused_write_address,                  /*  0x509B                                     */
    &unused_write_address,                  /*  0x509C                                     */
    &unused_write_address,                  /*  0x509D                                     */
    &unused_write_address,                  /*  0x509E                                     */
    &unused_write_address,                  /*  0x509F                                     */
    &unused_write_address,                  /*  0x50A0                                     */
    &unused_write_address,                  /*  0x50A1                                     */
    &unused_write_address,                  /*  0x50A2                                     */
    &unused_write_address,                  /*  0x50A3                                     */
    &unused_write_address,                  /*  0x50A4                                     */
    &unused_write_address,                  /*  0x50A5                                     */
    &unused_write_address,                  /*  0x50A6                                     */
    &unused_write_address,                  /*  0x50A7                                     */
    &unused_write_address,                  /*  0x50A8                                     */
    &unused_write_address,                  /*  0x50A9                                     */
    &unused_write_address,                  /*  0x50AA                                     */
    &unused_write_address,                  /*  0x50AB                                     */
    &unused_write_address,                  /*  0x50AC                                     */
    &unused_write_address,                  /*  0x50AD                                     */
    &unused_write_address,                  /*  0x50AE                                     */
    &unused_write_address,                  /*  0x50AF                                     */
    &unused_write_address,                  /*  0x50B0                                     */
    &unused_write_address,                  /*  0x50B1                                     */
    &unused_write_address,                  /*  0x50B2                                     */
    &unused_write_address,                  /*  0x50B3                                     */
    &unused_write_address,                  /*  0x50B4                                     */
    &unused_write_address,                  /*  0x50B5                                     */
    &unused_write_address,                  /*  0x50B6                                     */
    &unused_write_address,                  /*  0x50B7                                     */
    &unused_write_address,                  /*  0x50B8                                     */
    &unused_write_address,                  /*  0x50B9                                     */
    &unused_write_address,                  /*  0x50BA                                     */
    &unused_write_address,                  /*  0x50BB                                     */
    &unused_write_address,                  /*  0x50BC                                     */
    &unused_write_address,                  /*  0x50BD                                     */
    &unused_write_address,                  /*  0x50BE                                     */
    &unused_write_address,                  /*  0x50BF                                     */
    &unused_write_address,                  /*  0x50C0                                     */
    &unused_write_address,                  /*  0x50C1                                     */
    &unused_write_address,                  /*  0x50C2                                     */
    &unused_write_address,                  /*  0x50C3                                     */
    &unused_write_address,                  /*  0x50C4                                     */
    &unused_write_address,                  /*  0x50C5                                     */
    &unused_write_address,                  /*  0x50C6                                     */
    &unused_write_address,                  /*  0x50C7                                     */
    &unused_write_address,                  /*  0x50C8                                     */
    &unused_write_address,                  /*  0x50C9                                     */
    &unused_write_address,                  /*  0x50CA                                     */
    &unused_write_address,                  /*  0x50CB                                     */
    &unused_write_address,                  /*  0x50CC                                     */
    &unused_write_address,                  /*  0x50CD                                     */
    &unused_write_address,                  /*  0x50CE                                     */
    &unused_write_address,                  /*  0x50CF                                     */
    &unused_write_address,                  /*  0x50D0                                     */
    &unused_write_address,                  /*  0x50D1                                     */
    &unused_write_address,                  /*  0x50D2                                     */
    &unused_write_address,                  /*  0x50D3                                     */
    &unused_write_address,                  /*  0x50D4                                     */
    &unused_write_address,                  /*  0x50D5                                     */
    &unused_write_address,                  /*  0x50D6                                     */
    &unused_write_address,                  /*  0x50D7                                     */
    &unused_write_address,                  /*  0x50D8                                     */
    &unused_write_address,                  /*  0x50D9                                     */
    &unused_write_address,                  /*  0x50DA                                     */
    &unused_write_address,                  /*  0x50DB                                     */
    &unused_write_address,                  /*  0x50DC                                     */
    &unused_write_address,                  /*  0x50DD                                     */
    &unused_write_address,                  /*  0x50DE                                     */
    &unused_write_address,                  /*  0x50DF                                     */
    &unused_write_address,                  /*  0x50E0                                     */
    &unused_write_address,                  /*  0x50E1                                     */
    &unused_write_address,                  /*  0x50E2                                     */
    &unused_write_address,                  /*  0x50E3                                     */
    &unused_write_address,                  /*  0x50E4                                     */
    &unused_write_address,                  /*  0x50E5                                     */
    &unused_write_address,                  /*  0x50E6                                     */
    &unused_write_address,                  /*  0x50E7                                     */
    &unused_write_address,                  /*  0x50E8                                     */
    &unused_write_address,                  /*  0x50E9                                     */
    &unused_write_address,                  /*  0x50EA                                     */
    &unused_write_address,                  /*  0x50EB                                     */
    &unused_write_address,                  /*  0x50EC                                     */
    &unused_write_address,                  /*  0x50ED                                     */
    &unused_write_address,                  /*  0x50EE                                     */
    &unused_write_address,                  /*  0x50EF                                     */
    &unused_write_address,                  /*  0x50F0                                     */
    &unused_write_address,                  /*  0x50F1                                     */
    &unused_write_address,                  /*  0x50F2                                     */
    &unused_write_address,                  /*  0x50F3                                     */
    &unused_write_address,                  /*  0x50F4                                     */
    &unused_write_address,                  /*  0x50F5                                     */
    &unused_write_address,                  /*  0x50F6                                     */
    &unused_write_address,                  /*  0x50F7                                     */
    &unused_write_address,                  /*  0x50F8                                     */
    &unused_write_address,                  /*  0x50F9                                     */
    &unused_write_address,                  /*  0x50FA                                     */
    &unused_write_address,                  /*  0x50FB                                     */
    &unused_write_address,                  /*  0x50FC                                     */
    &g_50FD,                                 /*  0x50FD                                     */
    &g_50FE,                                 /*  0x50FE                                     */
    &unused_write_address,                  /*  0x50FF                                     */
};

void    register_load_hard_coded_values(void)
{
    g_5101.value_u16 = 0x0101;
    g_5102.value_u16 = 0xB0B0;
}

modbus_register_t *const   get_read_regs(uint16_t reg)
{
        
      return read_registers[reg];

};

modbus_register_t *const   get_write_regs(uint16_t reg)
{
        
      return write_register[reg];

};


