#ifndef SENSORS_H
#define SENSORS_H


#define SENSOR_READ_H    0x03
#define SENSOR_READ_I    0x04
#define SENSOR_WRITE     0x06


//DANFOSS
//Sensor Specific 
//==============================================================================================================================================
#define DANFOSS_ADDRESS   0x01

#define DANFOSS_REG_LFL   0x00
#define DANFOSS_REG_ERR   0x01   /* 0 - No Alarm, 1 - Alarm */
#define DANFOSS_REG_A_T   0x02   /* 180 default, range 0 - 1000 */
#define DANFOSS_REG_A_S   0x03   /* 0 - No error, */

#define DANFOSS_REG_ADD   0x03E8 


//T-O-D
//==============================================================================================================================================
#define TOD_ADDRESS       0x01

#define TOD_REG_OPM       0x01    /* OPERATING MODES: warm up 0x01, running 0x02, error 0x03 */
#define TOD_REG_STATUS    0x02    /* LSB - ob0000000x == LFL out of range, 0b000000x0 == Soft error, 0b00000x00 == Hard error */   
#define TOD_REG_LFL       0x03    

//Auto discovery message for this sensor is " 0x00 0x03 0x00 0x01 0x00 0x01 " crc 0xD4 0x1B " "




//===============================================================================================================================================




#endif
