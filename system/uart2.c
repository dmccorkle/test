#include    "system.h"
//#include    <string.h>
#include    "uart2.h"
#include    "crc.h"
#include    "../modbus_main.h"

#define     UART2_CONFIG_BUFFER_SIZE    40

/*  UART2 is the Main UART for communications with
 * the CLM Unit bus.  It, along with Timer0 controls
 * Modbus communications for slave operations.
 */

volatile static uint8_t input_buffer[UART2_CONFIG_BUFFER_SIZE] = {0};
volatile static uint8_t output_buffer[UART2_CONFIG_BUFFER_SIZE] = {0};

static  uint8_t     bytes_received_u8 = 0;
static  uint8_t     current_transmit_byte_u8 = 0;
static  uint8_t     bytes_to_transmit_u8 = 0;

static crc_t        uart2_crc;

static void _uart2_set_uart_enable(bool uart_enable)
{
    UART2_ENABLE = uart_enable;
    U2CON0bits.TXEN = 1;
}

void    uart2_send_byte(void)
{
    tmr0_reset_timer();
    _uart2_set_uart_enable(true);
    U2TXB = output_buffer[current_transmit_byte_u8++];
    crc_t tx_crc = uart2_calculate_crc(output_buffer[current_transmit_byte_u8 - 1]);
    if (current_transmit_byte_u8 == bytes_to_transmit_u8)
    {
        output_buffer[current_transmit_byte_u8] = tx_crc.crc_h_u8;
        output_buffer[current_transmit_byte_u8 + 1] = tx_crc.crc_l_u8;
    }
    PIE8bits.U2TXIE = 1;
}

void    uart2_initialize(void)
{
    PIE8bits.U2RXIE = 0;
    PIE8bits.U2TXIE = 0;
    
    U2RXPPS = 0x11;        /*    Maps RC1 to UART2 RX (default but doing it anyway)        */
    
    U2P1 = 0x0;
    U2P2 = 0x0;
    U2P3 = 0x0;
    U2CON0 = 0b00100000;
    U2CON1 = 0b10000000;
    U2CON2 = 0b00110000;  //or 0x30
    U2ERRIE = 0b10000000; //0x80
    U2ERRIR = 0x0;
    //U2CON0bits.TXEN = 1;
    //U2ERRIEbits.TXMTIE = 1;
    
    
    /*
     * GRB = Fosc/(16 x desired baud) - 1
     *      16000000/ (16 * 9600) - 1 = 103
     *      16000000/ (16 x (103 + 1) = 9615
     *      Error = |15|/9600 = 0.16%
    */
    
    U2BRGL = 0x67;
    U2BRGH = 0x0;
    
    PIR8bits.U2RXIF = 0;
    PIE8bits.U2RXIE = 1;
}

void    uart2_transmit_isr(void)
{
    tmr0_set_time(DELAY_1_8_MS);
    tmr0_reset_timer();
    PIR3bits.TMR0IF = 0;
    PIR8bits.U2TXIF = 0;
    modbus_main_set_state(MODBUS_NO_STATE);
    if (current_transmit_byte_u8 == (bytes_to_transmit_u8 + 2))
    {
        PIR8bits.U2TXIF = 0;
        
        bytes_to_transmit_u8 = 0;
        current_transmit_byte_u8 = 0;
        PIE8bits.U2TXIE = 0;
        for(int i = 0;i < UART2_CONFIG_BUFFER_SIZE; i++)
            input_buffer[i] = 0;
        //memset(input_buffer, 0x0, UART2_CONFIG_BUFFER_SIZE);
        modbus_main_set_state(MODBUS_TRANSMIT_DONE);        
        uart2_reset_crc();
    }
    else
    {
        uart2_send_byte();
    }
    
}

void    uart2_receive_isr(void)
{
    tmr0_set_time(DELAY_2_87_MS);
    tmr0_reset_timer();
    PIR8bits.U2RXIF = 0;
    
/* WARNING: May need to see corresponding U2ERRIE bits to enable
 * these
 */
    if (U2ERRIRbits.TXCIF || U2ERRIRbits.FERIF)
    {
        modbus_main_set_state(MODBUS_COLLISION);
        U2ERRIRbits.TXCIF = 0;
        U2ERRIRbits.FERIF = 0;
    }
    else
    {
        input_buffer[bytes_received_u8++] = U2RXB;
        uart2_calculate_crc(input_buffer[bytes_received_u8 - 1]);
        modbus_main_set_state(MODBUS_NO_STATE);
    }
    PIR8bits.U2TXIF = 0;
}

crc_t   uart2_get_crc(void)
{
    return uart2_crc;
}

void    uart2_reset_crc(void)
{
    uart2_crc.crc_u16 = MODBUS_CRC_RESET_VALUE;
}

crc_t   uart2_calculate_crc(uint8_t current_byte_u8)
{
    uint16_t m_crc = uart2_crc.crc_u16;
    uint8_t n_temp = (uint8_t) (current_byte_u8 ^ m_crc);
    m_crc >>= 8;
    m_crc ^= g_crc_table[n_temp];
    
    uart2_crc.crc_u16 = m_crc;
    return uart2_crc;    
}

void    uart2_set_bytes_received(uint8_t bytes_u8)
{
    bytes_received_u8 = bytes_u8;
}

void    uart2_set_receive_ready(void)
{
    PIE8bits.U2RXIE = 1;
    PIE8bits.U2TXIE = 0;
    U2CON0bits.RXEN = 1;
}

void    uart2_disable_interrupts(void)
{
    PIE8bits.U2RXIE = 0;
    PIE8bits.U2TXIE = 0;
}

uint8_t uart2_get_input_buffer_byte(uint8_t position)
{
    return input_buffer[position];
}

void    uart2_set_transmit_buffer_byte(uint8_t tx_byte, uint8_t position)
{
    output_buffer[position] = tx_byte;
}

void    uart2_set_bytes_to_transmit(uint8_t number)
{
    bytes_to_transmit_u8 = number;
}