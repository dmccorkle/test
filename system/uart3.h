#ifndef UART3_H
#define	UART3_H

void    uart3_send_byte(void);

void    uart3_initialize(void);
void    uart3_transmit_isr(void);
void    uart3_receive_isr(void);

crc_t   uart3_get_crc(void);
void    uart3_reset_crc(void);
crc_t   uart3_calculate_crc(uint8_t current_byte_u8);
void    uart3_set_bytes_received(uint8_t bytes_u8);
void    uart3_set_receive_ready(void);
void    uart3_disable_interrupts(void);
uint8_t uart3_get_input_buffer_byte(uint8_t position);
void    uart3_set_transmit_buffer_byte(uint8_t tx_byte, uint8_t position);
void    uart3_set_bytes_to_transmit(uint8_t number);

#endif	/* UART3_H */