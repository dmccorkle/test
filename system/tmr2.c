#include        "system.h"

#include        "tmr2.h"


/*
 *  FOSC = 4MHz
 *  FOSC/4 = 1MHz
 *  Tick = 1/((FOSC/4)/16) = 0.016ms
 *  Timer Duration = 0.016 * 255 = 4.08ms
 *  PR(1ms) = 1/4.08 (255) = 63 or 0xBB
 */

void    (*tmr2_interrupt_handler)(void);

void    _tmr2_set_interrupt_handler(void (*interrupt_handler)(void))
{
    tmr2_interrupt_handler = interrupt_handler;
}

void    _tmr2_default_interrupt(void)
{
    line_sync_execute();
}

void    tmr2_initialize(void)
{
    
    T2CLKCON = 0x01;
    T2HLT = 0x00;
    T2RST = 0x00;
    T2PR = 0xFF;
    T2TMR = 0x00;
    
    PIR3bits.TMR2IF = 0;
    PIE3bits.TMR2IE = 1;
    
    _tmr2_set_interrupt_handler(_tmr2_default_interrupt); 
    
    T2CON = 0b10100000;
    
    T2CONbits.TMR2ON = 1;
}

void    tmr2_isr(void)
{
    PIR3bits.TMR2IF = 0;
    T2PR = 0xFF;
    T2TMR = 0x00;
    T2CONbits.TMR2ON = 1;
    
    
    
    if (tmr2_interrupt_handler)
    {
        tmr2_interrupt_handler();
    }
}