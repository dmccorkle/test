#ifndef UART2_H
#define	UART2_H

void    uart2_send_byte(void);

void    uart2_initialize(void);
void    uart2_transmit_isr(void);
void    uart2_receive_isr(void);

crc_t   uart2_get_crc(void);
void    uart2_reset_crc(void);
crc_t   uart2_calculate_crc(uint8_t current_byte_u8);
void    uart2_set_bytes_received(uint8_t bytes_u8);
void    uart2_set_receive_ready(void);
void    uart2_disable_interrupts(void);
uint8_t uart2_get_input_buffer_byte(uint8_t position);
void    uart2_set_transmit_buffer_byte(uint8_t tx_byte, uint8_t position);
void    uart2_set_bytes_to_transmit(uint8_t number);

#endif	/* UART2_H */

