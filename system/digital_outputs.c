#include    "system.h"

#include    "digital_outputs.h"


#define _ON     1
#define _OFF    0

/*
 * For GPIO configuration see system.gpio.h  
 */

system_relay_t safety_relay = {
  .port = &LATA,
  .port_bit = 2,
  .flags = {
      .relay_rising_edge = false,
      .relay_on = false,
      .relay_previous = false
  },
  .status = RELAY_OFF  
};

void    digital_outputs_drive_safety_relay(bool rising_edge)
{
    if ((safety_relay.flags.relay_previous != safety_relay.flags.relay_on) &&
            (rising_edge == safety_relay.flags.relay_rising_edge))
    {
        if(safety_relay.status == RELAY_OFF)
        {
            *safety_relay.port |= 1 << safety_relay.port_bit;
            safety_relay.status = RELAY_ON;
        }
        else
        {
            *safety_relay.port &= !(1 << safety_relay.port_bit);
            safety_relay.status = RELAY_OFF;
        }
        safety_relay.flags.relay_previous = safety_relay.flags.relay_on;
        safety_relay.flags.relay_rising_edge = !rising_edge;
    }
}

void    digital_outputs_stage_safety_relay(bool status)
{
    safety_relay.flags.relay_on = status;
}


void    digital_outputs_drive_sensor_1_led(e_led_status_t status)
{
    if (LED_ON== status)
    {
        LED_S1 = _ON;
    }
    else
    {
        LED_S1 = _OFF;
    }
    
}

void    digital_outputs_drive_sensor_2_led(e_led_status_t status)
{
    if (LED_ON == status)
    {
        LED_S2 = _ON;
    }
    else
    {
        LED_S2 = _OFF;
    }
}