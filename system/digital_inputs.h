#ifndef DIGITAL_INPUTS_H
#define	DIGITAL_INPUTS_H

#include    <stdbool.h>

typedef union 
{
    struct
    {
        bool dipswitch_01   :   1;
        bool dipswitch_02   :   1;
    };
    uint8_t value_u8;
} dipswitch_t;

void    digital_inputs_sample(void);
void    digital_inputs_debounce(void);

#endif	/* DIGITAL_INPUTS_H */

