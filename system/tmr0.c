#include    "system.h"

#include    "tmr0.h"
#include    "../modbus_main.h"

/*
 * An important note about the 16bit timers,  unlike other timers with PIC18
 * and PIC24, in the Q Series, we will not be using Period compare.  So when
 * normally we would reset the timer, you will be setting the timer to the time
 * value you normally set in the PR register.  If anything, this simplifies things
 * as before you had to make sure you cleared your timer.
 */

volatile uint16_t tmr0_reload_value_u16 = 65536 - DELAY_20_MS ;

void    tmr0_initialize(void)
{
    T0CON1 = 0b01000011;
    
    TMR0H = tmr0_reload_value_u16 >> 8;
    TMR0L = (uint8_t) tmr0_reload_value_u16;;
    
    PIR3bits.TMR0IF = 0;
    PIE3bits.TMR0IE = 1;
    
    T0CON0 = 0b10010000;
    T0CON0bits.EN = 1;
}

void    tmr0_isr(void)
{
    PIR3bits.TMR0IF = 0;
    TMR0H = tmr0_reload_value_u16 >> 8;
    TMR0L = (uint8_t) tmr0_reload_value_u16;
    //UART2_ENABLE = !UART2_ENABLE;
    modbus_main_execute();
    
}

void    tmr0_set_time(uint16_t delay_u16)
{
    tmr0_reload_value_u16 = (uint16_t) (65536 - delay_u16);
}

void    tmr0_reset_timer(void)
{
    TMR0H = tmr0_reload_value_u16 >> 8;
    TMR0L = (uint8_t) tmr0_reload_value_u16;
}

void    tmr0_clear_interrupt(void)
{
    PIR3bits.TMR0IF = 0;
}