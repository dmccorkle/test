#include     "system.h"

#include    "digital_inputs.h"

/*
 * For GPIO configuration see system.gpio.h  
 */

static dipswitch_t switch_current;
static dipswitch_t switch_previous;

dipswitch_t     switch_debounced;

void    digital_inputs_sample(void)
{
    switch_current.dipswitch_01 = DIP_1;
    switch_current.dipswitch_02 = DIP_2;
    
}

void    digital_inputs_debounce(void)
{
    if (switch_current.value_u8 == switch_previous.value_u8)
    {
        switch_debounced.value_u8 = switch_current.value_u8;
    }
    switch_previous.value_u8 = switch_current.value_u8;
}