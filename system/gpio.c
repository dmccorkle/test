
#include    "system.h"
#include    "gpio.h"

/*
 * RA0 - ICSPDATA   ***Programming/Debugging
 * RA1 - ICSPCLK    ***Programming/Debugging
 * RA2 - Relay Out  DIGITAL OUTPUT
 * RA3 - MCLR       ***Programming/Debugging
 * RA4 - DIP2       DIGITAL INPUT
 * RA5 - DIP1       DIGITAL INPUT
 * 
 * RB4 - U2_TX      PPS UART2
 * RB5 - U1_RX      UART1 Rx
 * RB6 - U1_TX      PPS UART 1 Tx
 * RB7 - U1_DE      UART1 Driver Enable
 * 
 * RC0 - LINE SYNC  DIGITAL INPUT
 * RC1 - U2_RX      UART2 Rx
 * RC2 - U2_DE      UART2 Driver Enable
 * RC3 - U3_RX      UART3 Rx
 * RC4 - U3_TX      PPS UART3 Tx
 * RC5 - U3_DE      UART3 Driver Enable
 * RC6 - LED 2      DIGITAL OUTPUT
 * RC7 - LED 1      DIGITAL OUTPUT
 */

static void    _gpio_porta_initialize(void)
{
    PORTA = 0b00000000;
    LATA = 0U;
    ANSELA = 0U;
    TRISA = 0b00111011;
    
    
}

static void    _gpio_portb_initialize(void)
{
    PORTB = 0U;
    LATB = 0U;
    ANSELB = 0U;
    TRISB = 0b00101111;
    
    RB4PPS = 0x13;
    RB6PPS = 0x10;
}

static void    _gpio_portc_initialize(void)
{
    PORTC = 0U;
    LATC = 0U;
    ANSELC = 0U;
    TRISC = 0b00001011;
    
    RC4PPS = 0x16;
}

void    gpio_initialize(void)
{
    _gpio_porta_initialize();
    _gpio_portb_initialize();
    _gpio_portc_initialize();
    LED_S1 = 1;
    LED_S2 = 1;
    
   
}

