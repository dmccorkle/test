#include    "system.h"
//#include    <string.h>
#include    "uart1.h"
#include    "crc.h"
#include    "../modbus_master1.h"

#define     UART1_CONFIG_BUFFER_SIZE    40

/*  UART1 is the first UART for communications with
 * the A2L Sensors.  It, along with Timer1 controls
 * Modbus communications for master1 operations.
 */

volatile static uint8_t input_buffer1[UART1_CONFIG_BUFFER_SIZE] = {0};
volatile static uint8_t output_buffer1[UART1_CONFIG_BUFFER_SIZE] = {0};

static  uint8_t     bytes_received1_u8 = 0;
static  uint8_t     current_transmit_byte1_u8 = 0;
static  uint8_t     bytes_to_transmit1_u8 = 0;

static crc_t        uart1_crc;

static void _uart1_set_uart_enable(bool uart_enable)
{
    UART1_ENABLE = uart_enable;
    U1CON0bits.TXEN = 1;
}

void    uart1_send_byte(void)
{
    tmr1_reset_timer();
    _uart1_set_uart_enable(true);
    U1TXB = output_buffer1[current_transmit_byte1_u8++];
    crc_t tx_crc = uart1_calculate_crc(output_buffer1[current_transmit_byte1_u8 - 1]);
    if (current_transmit_byte1_u8 == bytes_to_transmit1_u8)
    {
        output_buffer1[current_transmit_byte1_u8] = tx_crc.crc_h_u8;
        output_buffer1[current_transmit_byte1_u8 + 1] = tx_crc.crc_l_u8;
    }
    PIE4bits.U1TXIE = 1;
}

void    uart1_initialize(void)
{
    PIE4bits.U1RXIE = 0;
    PIE4bits.U1TXIE = 0;
    
    U1RXPPS = 0x0D;
    
    U1P1 = 0x0;
    U1P2 = 0x0;
    U1P3 = 0x0;
    U1CON0 = 0b00100000;
    U1CON1 = 0b10000000;
    U1CON2 = 0b00110000;  //or 0x30
    U1ERRIE = 0b10000000; //0x80
    U1ERRIR = 0x0;
    
    
    /*
     * GRB = Fosc/(16 x desired baud) - 1
     *      16000000/ (16 * 9600) - 1 = 103
     *      16000000/ (16 x (103 + 1) = 9615
     *      Error = |15|/9600 = 0.16%
    */
    
    U2BRGL = 0x67;
    U2BRGH = 0x0;
    
    PIR4bits.U1RXIF = 0;
    PIE4bits.U1RXIE = 1;
}

void    uart1_transmit_isr(void)
{
    tmr1_set_time(DELAY_1_8_MS);
    tmr1_reset_timer();
    PIR3bits.TMR1IF = 0;
    PIR4bits.U1TXIF = 0;
    modbus_master1_set_state(MODBUS_NO_STATE);
    if (current_transmit_byte1_u8 == (bytes_to_transmit1_u8 + 2))
    {
        PIR4bits.U1TXIF = 0;
        
        bytes_to_transmit1_u8 = 0;
        current_transmit_byte1_u8 = 0;
        PIE4bits.U1TXIE = 0;
        for(int i = 0; i < UART1_CONFIG_BUFFER_SIZE; i++)
            input_buffer1[i] = 0;
       
        modbus_master1_set_state(MODBUS_TRANSMIT_DONE);
        uart1_reset_crc();
    }
    else
    {
        uart1_send_byte();
    }
    
}

void    uart1_receive_isr(void)
{
    tmr1_set_time(DELAY_2_87_MS);
    tmr1_reset_timer();
    PIR4bits.U1RXIF = 0;
    
/* WARNING: May need to see corresponding U1ERRIE bits to enable
 * these
 */
    if(U1ERRIRbits.TXCIF || U1ERRIRbits.FERIF)
    {
        modbus_master1_set_state(MODBUS_COLLISION);
        U1ERRIRbits.TXCIF = 0;
        U1ERRIRbits.FERIF = 0;
    }
    else
    {
        input_buffer1[bytes_received1_u8++] = U1RXB;
        uart1_calculate_crc(input_buffer1[bytes_received1_u8 - 1]);
        modbus_master1_set_state(MODBUS_NO_STATE);
    }
    PIR4bits.U1TXIF = 0;
}

crc_t   uart1_get_crc(void)
{
    return uart1_crc;
}

void    uart1_reset_crc(void)
{
    uart1_crc.crc_u16 = MODBUS_CRC_RESET_VALUE;
}

crc_t   uart1_calculate_crc(uint8_t current_byte_u8)
{
    uint16_t m_crc = uart1_crc.crc_u16;
    uint8_t n_temp = (uint8_t) (current_byte_u8 ^ m_crc);
    m_crc >>= 8;
    m_crc ^= g_crc_table[n_temp];
    
    uart1_crc.crc_u16 = m_crc;
    return uart1_crc;    
}

void    uart1_set_bytes_received(uint8_t bytes_u8)
{
    bytes_received1_u8 = bytes_u8;
}

void    uart1_set_receive_ready(void)
{
    PIE4bits.U1RXIE = 1;
    PIE4bits.U1TXIE = 0;
    U1CON0bits.RXEN = 1;
}

void    uart1_disable_interrupts(void)
{
    PIE4bits.U1RXIE = 0;
    PIE4bits.U1TXIE = 0;
}

uint8_t uart1_get_input_buffer_byte(uint8_t position)
{
    return input_buffer1[position];
}

void    uart1_set_transmit_buffer_byte(uint8_t tx_byte, uint8_t position)
{
    output_buffer1[position] = tx_byte;
}

void    uart1_set_bytes_to_transmit(uint8_t number)
{
    bytes_to_transmit1_u8 = number;
}