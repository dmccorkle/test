#ifndef TMR2_H
#define	TMR2_H

void    tmr2_initialize(void);
void    tmr2_isr(void);

#endif	/* TMR2_H */

