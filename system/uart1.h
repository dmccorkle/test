#ifndef UART1_H
#define	UART1_H

void    uart1_send_byte(void);

void    uart1_initialize(void);
void    uart1_transmit_isr(void);
void    uart1_receive_isr(void);

crc_t   uart1_get_crc(void);
void    uart1_reset_crc(void);
crc_t   uart1_calculate_crc(uint8_t current_byte_u8);
void    uart1_set_bytes_received(uint8_t bytes_u8);
void    uart1_set_receive_ready(void);
void    uart1_disable_interrupts(void);
uint8_t uart1_get_input_buffer_byte(uint8_t position);
void    uart1_set_transmit_buffer_byte(uint8_t tx_byte, uint8_t position);
void    uart1_set_bytes_to_transmit(uint8_t number);

#endif	/* UART1_H */