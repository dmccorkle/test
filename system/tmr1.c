#include    "system.h"

#include    "tmr1.h"
#include    "../modbus_master1.h"

volatile uint16_t tmr1_reload_value_u16 = 65536 - DELAY_20_MS;

void    tmr1_initialize(void)
{
    T1GCON = 0b00000000;
    T1GATE = 0x00;
    
    T1CLK = 0x01;
    
    TMR1H = tmr1_reload_value_u16 >> 8;
    TMR1L = (uint8_t)tmr1_reload_value_u16;
    
    PIR3bits.TMR1IF = 0;
    PIE3bits.TMR1IE = 1;
    
    T1CON = 0b00110001;        
}

void    tmr1_isr(void)
{
    PIR3bits.TMR1IF = 0;
    TMR1H = tmr1_reload_value_u16 >> 8;
    TMR1L = (uint8_t)tmr1_reload_value_u16;
    //UART1_ENABLE = !UART1_ENABLE;
    modbus_master1_execute();
}

void    tmr1_set_time(uint16_t delay_u16)
{
    tmr1_reload_value_u16 = (uint16_t) (65536 - delay_u16);
}

void    tmr1_reset_timer(void)
{
    TMR1H = tmr1_reload_value_u16 >> 8;
    TMR1L = (uint8_t) tmr1_reload_value_u16;
}

void    tmr1_clear_interrupt(void)
{
    PIR3bits.TMR1IF = 0;
}

