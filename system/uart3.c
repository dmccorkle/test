#include    "system.h"

#include    "uart3.h"
#include    "crc.h"
#include    "../modbus_master2.h"
#include    "../register.h"



#define     UART3_CONFIG_BUFFER_SIZE    40

/*  UART3 is the first UART for communications with
 * the A2L Sensors.  It, along with Timer3 controls
 * Modbus communications for master2 operations.
 */

volatile static uint8_t input_buffer2[UART3_CONFIG_BUFFER_SIZE] = {0};
volatile static uint8_t output_buffer2[UART3_CONFIG_BUFFER_SIZE] = {0};

static  uint8_t     bytes_received2_u8 = 0;
static  uint8_t     current_transmit_byte2_u8 = 0;
static  uint8_t     bytes_to_transmit2_u8 = 0;

static crc_t        uart3_crc;

static void _uart3_set_uart_enable(bool uart_enable)
{
    UART3_ENABLE = uart_enable;
    U3CON0bits.TXEN = 1;
}

void    uart3_send_byte(void)
{
    tmr3_reset_timer();
    _uart3_set_uart_enable(true);
    U3TXB = output_buffer2[current_transmit_byte2_u8++];
    crc_t tx_crc = uart3_calculate_crc(output_buffer2[current_transmit_byte2_u8 - 1]);
    if (current_transmit_byte2_u8 == bytes_to_transmit2_u8)
    {
        output_buffer2[current_transmit_byte2_u8] = tx_crc.crc_h_u8;
        output_buffer2[current_transmit_byte2_u8 + 1] = tx_crc.crc_l_u8;
    }
    PIE9bits.U3TXIE = 1;
}

void    uart3_initialize(void)
{
    PIE9bits.U3RXIE = 0;
    PIE9bits.U3TXIE = 0;
    
    U3RXPPS = 0x13;
    
    U3P1 = 0x0;
    U3P2 = 0x0;
    U3P3 = 0x0;
    U3CON0 = 0b00100000;
    U3CON1 = 0b10000000;
    U3CON2 = 0b00110000;  //or 0x30
    U3ERRIE = 0b10000000; //0x80
    U3ERRIR = 0x0;
    
    
    /*
     * GRB = Fosc/(16 x desired baud) - 1
     *      16000000/ (16 * 9600) - 1 = 103
     *      16000000/ (16 x (103 + 1) = 9615
     *      Error = |15|/9600 = 0.16%
    */
    
    U3BRGL = 0x67;
    U3BRGH = 0x0;
    
    PIR9bits.U3RXIF = 0;
    PIE9bits.U3RXIE = 1;
}

void    uart3_transmit_isr(void)
{
    tmr3_set_time(DELAY_1_8_MS);
    tmr3_reset_timer();
    PIR4bits.TMR3IF = 0;
    PIR9bits.U3TXIF = 0;
    
    if (current_transmit_byte2_u8 == (bytes_to_transmit2_u8 + 3))
    {
        PIR9bits.U3TXIF = 0;
        
        bytes_to_transmit2_u8 = 0;
        current_transmit_byte2_u8 = 0;
        PIE9bits.U3TXIE = 0;
        for(int i = 0; i < UART3_CONFIG_BUFFER_SIZE; i++)
            input_buffer2[i] = 0;
             
        
        uart3_reset_crc();
        mm2_flag = false;
        _uart3_set_uart_enable(false);
        uart3_set_receive_ready();
    }
    else
    {
        uart3_send_byte();
    }
    
}

void    uart3_receive_isr(void)
{
    modbus_register_t *temp_reg;
    uint16_t reg;
    
    tmr3_set_time(DELAY_2_87_MS);
    tmr3_reset_timer();
    PIR9bits.U3RXIF = 0;
    
/* WARNING: May need to see corresponding U1ERRIE bits to enable
 * these
 */
    if (U3ERRIRbits.TXCIF || U3ERRIRbits.FERIF)
    {
            
        U3ERRIRbits.TXCIF = 0;
        U3ERRIRbits.FERIF = 0;
    }
    else
    {
        input_buffer2[bytes_received2_u8++] = U3RXB;
        uart3_calculate_crc(input_buffer2[bytes_received2_u8 - 1]);
        
        if(bytes_received2_u8 == 13 && uart3_crc.crc_u16 == 0 ){
                                  
            reg = (input_buffer2[3] << 8) | input_buffer2[4];
            temp_reg = get_read_regs(0x11);
            temp_reg->value_u16 = reg;
            
            reg = (input_buffer2[5] << 8) | input_buffer2[6];
            temp_reg = get_read_regs(0x0C);
            temp_reg->value_u16 = reg;
            
            reg = (input_buffer2[7] << 8) | input_buffer2[8];
            temp_reg = get_read_regs(0x06);
            temp_reg->value_u16 = reg;
            
            reg = (input_buffer2[9] << 8) | input_buffer2[10];
            temp_reg = get_read_regs(0x0D);
            if(reg == 1)
                temp_reg->value_u16 = 0x0200;
            else
                temp_reg->value_u16 = 0x0000;
            
            for(char i = 0; i < UART3_CONFIG_BUFFER_SIZE; i++)
                output_buffer2[i] = 0;
            
            _uart3_set_uart_enable(true);            
            
        }
        
    }
    PIR9bits.U3TXIF = 0;
}

crc_t   uart3_get_crc(void)
{
    return uart3_crc;
}

void    uart3_reset_crc(void)
{
    uart3_crc.crc_u16 = MODBUS_CRC_RESET_VALUE;
}

crc_t   uart3_calculate_crc(uint8_t current_byte_u8)
{
    uint16_t m_crc = uart3_crc.crc_u16;
    uint8_t n_temp = (uint8_t) (current_byte_u8 ^ m_crc);
    m_crc >>= 8;
    m_crc ^= g_crc_table[n_temp];
    
    uart3_crc.crc_u16 = m_crc;
    return uart3_crc;    
}

void    uart3_set_bytes_received(uint8_t bytes_u8)
{
    bytes_received2_u8 = bytes_u8;
}

void    uart3_set_receive_ready(void)
{
    PIE9bits.U3RXIE = 1;
    PIE9bits.U3TXIE = 0;
    U3CON0bits.RXEN = 1;
}

void    uart3_disable_interrupts(void)
{
    PIE9bits.U3RXIE = 0;
    PIE9bits.U3TXIE = 0;
}

uint8_t uart3_get_input_buffer_byte(uint8_t position)
{
    return input_buffer2[position];
}

void    uart3_set_transmit_buffer_byte(uint8_t tx_byte, uint8_t position)
{
    output_buffer2[position] = tx_byte;
}

void    uart3_set_bytes_to_transmit(uint8_t number)
{
    bytes_to_transmit2_u8 = number;
}