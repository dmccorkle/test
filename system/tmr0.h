#ifndef TMR0_H
#define	TMR0_H

void    tmr0_initialize(void);
void    tmr0_isr(void);
void    tmr0_set_time(uint16_t delay_u16);
void    tmr0_reset_timer(void);
void    tmr0_clear_interrupt(void);

#endif	/* TMR0_H */

