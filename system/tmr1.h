#ifndef TMR1_H
#define	TMR1_H

void    tmr1_initialize(void);
void    tmr1_isr(void);
void    tmr1_set_time(uint16_t delay_u16);
void    tmr1_reset_timer(void);
void    tmr1_clear_interrupt(void);

#endif	/* TMR1_H */

