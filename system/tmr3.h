#ifndef TMR3_H
#define	TMR3_H

void    tmr3_initialize(void);
void    tmr3_isr(void);
void    tmr3_set_time(uint16_t delay_u16);
void    tmr3_reset_timer(void);
void    tmr3_clear_interrupt(void);

#endif	/* TMR3_H */

