#include    "system.h"
#include    "device_config.h"

/*  CONFIG1                                                                     */
#pragma config FEXTOSC = OFF            /*  External Oscillator not enabled     */
#pragma config RSTOSC = HFINTOSC_1MHZ   /*  Reset HFINTOSC with HFFRQ = 4 MHz 
                                            and CDIV = 4:1                      */

/*  CONFIG2                                                                     */
#pragma config CLKOUTEN = OFF           /*  CLKOUT function is disabled         */
#pragma config PR1WAY = ON              /*  PRLOCKED bit cleared and set once   */
#pragma config CSWEN = ON               /*  Writing to NOSC and NDIV is allowed */
#pragma config FCMEN = ON               /*  Fail-Safe Clock Monitor enabled     */
#pragma config FCMENP = ON              /*  flag FSCMP bit and OSFIF interrupt  
                                            on EXTOSC failure.                  */
#pragma config FCMENS = ON              /*  flag FSCMP bit and OSFIF interrupt  
                                            on SOSC failure.                    */

/*  CONFIG3                                                                     */
#pragma config MCLRE = EXTMCLR          /*  RE3 pin function is MCLR            */
#pragma config PWRTS = PWRT_OFF         /*  PWRT is disabled                    */
#pragma config MVECEN = OFF             /*  does not use vector table to        
                                            prioritze interrupts                */
#pragma config IVT1WAY = ON             /*  IVTLOCKED bit cleared and set once  */
#pragma config LPBOREN = OFF            /*  Low-Power BOR disabled              */
#pragma config BOREN = SBORDIS          /*  SBOREN bit is ignored               */

/*  CONFIG4                                                                     */
#pragma config BORV = VBOR_1P9          /*  Brown-out Reset Voltage set to 1.9V */
#pragma config ZCD = OFF                /*  ZCD enabled by ZCDSEN bit of ZCDCON */
#pragma config PPS1WAY = ON             /*  PPS registers locked after 1 
                                            clear/set cycle                     */
#pragma config STVREN = ON              /*  Stack full/underflow cause Reset    */
#pragma config LVP = ON                 /*  MCLR/VPP pin function is MCLR. 
                                            MCLRE configuration bit is ignored  */
#pragma config XINST = OFF              /*  Extended Instruction Set and Indexed 
                                            Addressing Mode disabled            */

/*  CONFIG5                                                                     */
#pragma config WDTCPS = WDTCPS_2        /*  WDT Period Divider ratio 1:128      */
#pragma config WDTE = OFF                /*  WDT enabled regardless of sleep; 
                                            SWDTEN is ignored                   */

/*  CONFIG6                                                                     */
#pragma config WDTCWS = WDTCWS_7        /*  WDT Window Select always open (100%)
                                            software control
                                            keyed access not required           */
#pragma config WDTCCS = SC              /*  WDT input clock Software Control    */

/*  CONFIG7                                                                     */
#pragma config BBSIZE = BBSIZE_512      /*  Boot Block size is 512 words        */
#pragma config BBEN = OFF               /*  Boot block disabled                 */
#pragma config SAFEN = OFF              /*  SAF disabled                        */
#pragma config DEBUG = OFF              /*  Background Debugger disabled        */

/*  CONFIG8                                                                     */
#pragma config WRTB = OFF               /*  Boot Block not Write protected      */
#pragma config WRTC = OFF               /*  Configuration registers not Write 
                                            protected                           */
#pragma config WRTD = OFF               /*  Data EEPROM not Write protected     */
#pragma config WRTSAF = OFF             /*  SAF not Write Protected             */
#pragma config WRTAPP = OFF             /*  Application Block not write 
                                            protected                           */

/*  CONFIG9                                                                     */
#pragma config CP = OFF                 /*  PFM and Data EEPROM code protection 
                                            disabled                            */

