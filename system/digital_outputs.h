#ifndef DIGITAL_OUTPUTS_H
#define	DIGITAL_OUTPUTS_H

#include    <stdbool.h>

typedef enum
{
    RELAY_OFF, RELAY_ON
} e_relay_status_t;

typedef enum 
{
    LED_ON, LED_OFF
} e_led_status_t;

typedef struct
{
    bool    relay_on    :   1;
    bool    relay_previous  :   1;
    bool    relay_rising_edge   :   1;
} relay_status_flags_t;

typedef struct
{
    volatile uint8_t*   port;
    const   uint8_t port_bit;
    volatile e_relay_status_t status;
    volatile relay_status_flags_t flags;    
} system_relay_t;

void    digital_outputs_stage_safety_relay(bool status);
void    digital_outputs_drive_safety_relay(bool rising_edge);
void    digital_outputs_drive_sensor_1_led(e_led_status_t status);
void    digital_outputs_drive_sensor_2_led(e_led_status_t status);

#endif	/* DIGITAL_OUTPUTS_H */

