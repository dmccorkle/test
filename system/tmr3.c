#include    "system.h"

#include    "tmr3.h"
#include    "../modbus_master2.h"

volatile uint16_t tmr3_reload_value_u16 = 65536 - DELAY_20_MS; 

void    tmr3_initialize(void)
{
    T3GCON = 0x00;
    T3GATE = 0x00;
    
    T3CLK = 0x01;
    
    TMR3H = tmr3_reload_value_u16 >> 8;
    TMR3L = (uint8_t)tmr3_reload_value_u16;
    
    PIR4bits.TMR3IF = 0;
    PIE4bits.TMR3IE = 1;
    
    T3CON = 0b00110001;
}

void    tmr3_isr(void)
{
    PIR4bits.TMR3IF = 0;
    TMR3H = tmr3_reload_value_u16 >> 8;
    TMR3L = (uint8_t)tmr3_reload_value_u16;
    //UART3_ENABLE = !UART3_ENABLE;
    //if(mm2_flag == true)
    //    uart3_send_byte();
    
}

void    tmr3_set_time(uint16_t delay_u16)
{
    tmr3_reload_value_u16 = (uint16_t) (65536 - delay_u16);
}

void    tmr3_reset_timer(void)
{
    TMR3H = tmr3_reload_value_u16 >> 8;
    TMR3L = (uint8_t) tmr3_reload_value_u16;
}

void    tmr3_clear_interrupt(void)
{
    PIR4bits.TMR3IF = 0;
}

