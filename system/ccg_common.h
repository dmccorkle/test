#ifndef CCG_COMMON_H
#define	CCG_COMMON_H

#define     DELAY_1_8_MS        900U
#define     DELAY_2_13_MS       1065U
#define     DELAY_2_87_MS       1435U
#define     DELAY_20_MS         10000U
#define     DELAY_50_MS         25000U

#define     MODBUS_READ_REGISTER        0x03
#define     MODBUS_WRITE_REGISTER       0x10

#define     MODBUS_PACKET_ADDRESS       0U
#define     MODBUS_PACKET_COMMAND       1U
#define     MODBUS_PACKET_REGISTER_HIGH 2U
#define     MODBUS_PACKET_REGISTER_LOW  3U
#define     MODBUS_PACKET_NUMBER_OF_REGISTERS   5U
#define     MODBUS_PACKET_NUMBER_OF_BYTES       6U

#define     MODBUS_CRC_RESET_VALUE      0xFFFF



#endif	/* CCG_COMMON_H */

