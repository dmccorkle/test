#include    "system.h"
#include    "interrupt.h"

void    interrupt_initialize(void)
{
    INTCON0bits.IPEN = 1;       /*  Enable Interrupt Priority Vectors           */
    INTCON0bits.GIEH = 1;
    INTCON0bits.GIEL = 1;
    
    
    IPR3bits.TMR0IP = 1;        /*  Timer 0 Low Priority Interrupt (MODBUS 1)           */
    IPR3bits.TMR2IP = 0;        /*  Timer 2 High Priority Interrupt (SYS)               */  
    IPR3bits.TMR1IP = 1;        /*  Timer 1 High Priority Interrupt (MODBUS 2)          */
    IPR4bits.TMR3IP = 1;        /*  Timer 3 High Priority Interrupt (MODBUS 3)          */
    IPR8bits.U2IP = 1;          /*  UART 2 (MAIN) High Priority Interrupt               */
    IPR4bits.U1IP = 1;          /*  UART 1 (MASTER 1) High Priority Interrupt           */
    IPR9bits.U3IP = 1;          /*  UART 3 (MASTER 2) High Priority Interrupt           */
}


void    __interrupt(high_priority) interrupt_interrupt_manager_high(void)
{
    
    if (PIE9bits.U3TXIE && PIR9bits.U3TXIF)
    {
        uart3_transmit_isr();
    }
    else if (PIE9bits.U3RXIE && PIR9bits.U3RXIF)
    {
        uart3_receive_isr();
    }
    else if (PIE8bits.U2TXIE && PIR8bits.U2TXIF)
    {
        uart2_transmit_isr();
    }
    else if (PIE8bits.U2RXIE && PIR8bits.U2RXIF)
    {
        uart2_receive_isr();
    }
    else if (PIE4bits.U1TXIE && PIR4bits.U1TXIF)
    {
        uart1_transmit_isr();
    }
    else if (PIE4bits.U1RXIE && PIR4bits.U1RXIF)
    {
        uart1_receive_isr();
    }    
    else if (PIE3bits.TMR0IE && PIR3bits.TMR0IF)
    {
        tmr0_isr();
    }    
    else if (PIE4bits.TMR3IE && PIR4bits.TMR3IF)
    {
        tmr3_isr();
    }
      else if (PIE3bits.TMR1IE && PIR3bits.TMR1IF)
    {
        tmr1_isr();
    }
    else
    {
        
    }
}

void    __interrupt(low_priority) interrupt_interrupt_manager_low(void)
{
    if (PIE3bits.TMR2IE && PIR3bits.TMR2IF)
    {
        tmr2_isr();
    }  
    else 
    {
        /*  Unhandled Interrupt                                                   */
    }
}