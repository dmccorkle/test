#ifndef LINE_SYNC_H
#define	LINE_SYNC_H

typedef union
{
    struct
    {
        bool    sync    :   1;
        bool    previous_sync   :   1;
        bool    no_sync         :   1;
        bool    settle          :   1;
        bool    tenth_second    :   1;
        bool    fifty_hz        :   1;
    };
    uint8_t slot_flag_value_u8;
} slot_flags_t;

void    line_sync_execute(void);
bool    line_sync_tenth_second_check(void);
void    line_sync_clear_tenth_second_flag(void);

#endif	/* LINE_SYNC_H */

