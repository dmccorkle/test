#ifndef IO_DEFINE_H
#define	IO_DEFINE_H

#define     SAFETY_RELAY            LATAbits.LATA2
#define     DIP_1                   PORTAbits.RA4
#define     DIP_2                   PORTAbits.RA5

#define     UART1_ENABLE            LATBbits.LATB7

#define     LINE_SYNC_IN            PORTCbits.RC0
#define     UART2_ENABLE            LATCbits.LATC2
#define     UART3_ENABLE            LATCbits.LATC5
#define     LED_S2                  LATCbits.LATC6
#define     LED_S1                  LATCbits.LATC7

#endif	/* IO_DEFINE_H */

