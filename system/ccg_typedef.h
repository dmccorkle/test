#ifndef CCG_TYPEDEF_H
#define	CCG_TYPEDEF_H

#include    <stdint.h>
#include    <stdbool.h>

typedef union
{
    struct
    {
        uint8_t crc_h_u8;
        uint8_t crc_l_u8;
    };
    uint16_t crc_u16;
} crc_t;

typedef union
{
    struct
    {
        union 
        {
            uint8_t lsb_u8;
            int8_t  lsb_s8;
        };
        union
        {
            uint8_t msb_u8;
            int8_t  msb_s8;
        };
    };
    struct
    {
        bool    bit_00_b;
        bool    bit_01_b;
        bool    bit_02_b;
        bool    bit_03_b;
        bool    bit_04_b;
        bool    bit_05_b;
        bool    bit_06_b;
        bool    bit_07_b;
        bool    bit_08_b;
        bool    bit_09_b;
        bool    bit_10_b;
        bool    bit_11_b;
        bool    bit_12_b;
        bool    bit_13_b;
        bool    bit_14_b;
        bool    bit_15_b;
    };
    uint16_t    value_u16;
    int16_t     value_s16;
} modbus_register_t;

typedef enum {
    MODBUS_NO_STATE = 0, MODBUS_COLLISION = 1, MODBUS_IDLE = 2, MODBUS_RECEIVE_DONE = 3, MODBUS_TRANSMIT_PENDING = 4, MODBUS_TRANSMIT_DONE = 5
} modbus_state_t;

#endif	/* CCG_TYPEDEF_H */

