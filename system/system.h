#ifndef SYSTEM_H
#define	SYSTEM_H
#include    <xc.h>

#include    "ccg_typedef.h"
#include    "ccg_common.h"

#include    "device_config.h"
#include    "interrupt.h"
#include    "gpio.h"
#include    "digital_inputs.h"
#include    "digital_outputs.h"
#include    "io_define.h"
#include    "tmr2.h"
#include    "tmr0.h"
#include    "uart2.h"
#include    "tmr1.h"
#include    "uart1.h"
#include    "tmr3.h"
#include    "uart3.h"
#include    "line_sync.h"

extern char mm2_flag;

void    system_initialize(void);

#endif	/* SYSTEM_H */

