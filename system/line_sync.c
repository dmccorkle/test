#include    "system.h"

#include    "line_sync.h"

#define     LOSS_OF_LINE_SYNC_LIMIT     100
#define     LINE_SYNC_50HZ_LIMIT        19

typedef enum
{
    SLOT_WAIT_FOR_RISING_EDGE   =   0,
    SLOT_01                     =   1,
    SLOT_02                     =   2,
    SLOT_WAIT_FOR_FALLING_EDGE  =   3,
    SLOT_04                     =   4,
    SLOT_05                     =   5,
    SLOT_06                     =   6,
    SLOT_07                     =   7,
    SLOT_MAX_SLOTS              =   7
    
} e_slot_state_t;

static e_slot_state_t   slot_state = SLOT_WAIT_FOR_RISING_EDGE;
static slot_flags_t slot_flags;
static uint8_t  rising_edge_timer_u8;
static uint16_t no_line_sync_u16;
static uint16_t line_sync_timer_u16;

bool    _line_sync_verify(void)
{
    bool    line_sync_lost = false;
    if (LOSS_OF_LINE_SYNC_LIMIT <= no_line_sync_u16)
    {
        line_sync_lost = true;
    }
    else if (0xFFFF > no_line_sync_u16)
    {
        no_line_sync_u16++;
    }
    return line_sync_lost;
}

void    _line_sync_exit(void)
{
    slot_flags.no_sync = _line_sync_verify();
}

bool    _line_sync_50Hz(void)
{
    if (LINE_SYNC_50HZ_LIMIT < rising_edge_timer_u8)
    {
        rising_edge_timer_u8 = 0;
        return true;
    }
    rising_edge_timer_u8 = 0;
    return false;
}

void    _line_sync_rising_edge_detect(void)
{
    if (slot_flags.sync && slot_flags.previous_sync)
    {
        no_line_sync_u16 = 0;
        slot_state = SLOT_01;
        slot_flags.fifty_hz = _line_sync_50Hz();
        digital_outputs_drive_safety_relay(true);
    }
    _line_sync_exit();
}

void    _line_sync_falling_edge_detect(void)
{
    if (!slot_flags.sync && !slot_flags.previous_sync)
    {
        slot_state = SLOT_04;
        digital_outputs_drive_safety_relay(false);
    }
    _line_sync_exit();
}

void    _line_sync_digital_inputs(void)
{
    slot_state++;
    digital_inputs_sample();
    digital_inputs_debounce();
    _line_sync_exit();
}

void    _line_sync_empty_slot(void)
{
    slot_state = SLOT_MAX_SLOTS == slot_state ?
        SLOT_WAIT_FOR_RISING_EDGE :
        slot_state + 1;
    _line_sync_exit();
}

bool    _line_sync_tenth_second_capture(void)
{
    uint8_t limit = slot_flags.fifty_hz ? 4 : 5;
    if (line_sync_timer_u16 >= limit)
    {
        line_sync_timer_u16 = 0;
        return true;
    }
    else if (0xFFFF > line_sync_timer_u16)
    {
        line_sync_timer_u16++;
    }
    return false;
}

void    line_sync_execute(void)
{
    slot_flags.previous_sync = slot_flags.sync ? 
        true : false;
    slot_flags.sync = LINE_SYNC_IN ?
        true : false;
    
    rising_edge_timer_u8 = 255U > rising_edge_timer_u8 ? 
        rising_edge_timer_u8 + 1 :
        255U;
    
    switch (slot_state)
    {
        case    SLOT_WAIT_FOR_RISING_EDGE:
            _line_sync_rising_edge_detect();
            break;
        case    SLOT_01:
            _line_sync_empty_slot();
            break;
        case    SLOT_02:
            slot_flags.tenth_second = _line_sync_tenth_second_capture();
            _line_sync_empty_slot();
            break;
        case    SLOT_WAIT_FOR_FALLING_EDGE:
            _line_sync_falling_edge_detect();
            break;
        case    SLOT_04:
            _line_sync_digital_inputs();
            break;
        case    SLOT_05:
            _line_sync_empty_slot();
            break;
        case    SLOT_06:
            _line_sync_empty_slot();
            break;
        case    SLOT_07:
            _line_sync_empty_slot();
            break;
    }
}

bool    line_sync_tenth_second_check(void)
{
    return slot_flags.tenth_second;
}

void    line_sync_clear_tenth_second_flag(void)
{
    slot_flags.tenth_second = false;
}