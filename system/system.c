#include    "system.h"

char    mm2_flag = false; 

void    _system_pmd_initialize(void)
{
    // CLKRMD CLKR enabled; SYSCMD SYSCLK enabled; SCANMD SCANNER enabled; LVDMD HLVD enabled; FVRMD FVR enabled; IOCMD IOC enabled; CRCMD CRC enabled; 
    PMD0 = 0x00;
    // ZCDMD ZCD enabled; TMR0MD TMR0 enabled; TMR1MD TMR1 enabled; TMR4MD TMR4 enabled; SMT1MD SMT1 enabled; TMR2MD TMR2 enabled; TMR3MD TMR3 enabled; CM1MD CM1 enabled; 
    PMD1 = 0x00;
    // NCO1MD NCO1 enabled; ADCMD ADC enabled; DSM1MD DSM enabled; CWG1MD CWG1 enabled; ACTMD ACT enabled; CM2MD CM2 enabled; DAC1MD DAC1 enabled; 
    PMD2 = 0x00;
    // PWM2MD PWM2 enabled; PWM1MD PWM1 enabled; PWM3MD PWM3 enabled; SPI2MD SPI2 enabled; SPI1MD SPI1 enabled; U2MD UART2 enabled; U1MD UART1 enabled; I2C1MD I2C1 enabled; 
    PMD3 = 0x00;
    // CLC3MD CLC3 enabled; CLC4MD CLC4 enabled; DMA1MD DMA1 enabled; DMA2MD DMA2 enabled; DMA3MD DMA3 enabled; CLC1MD CLC1 enabled; CLC2MD CLC2 enabled; 
    PMD4 = 0x00;
    // DMA4MD DMA4 enabled; OPAMD OPA enabled; DAC2MD DAC2 enabled; 
    PMD5 = 0x00;

}

void    _system_oscillator_initialize(void)
{
    // NOSC HFINTOSC; NDIV 4; 
    OSCCON1 = 0x60;
    // CSWHOLD may proceed; SOSCPWR Low power; 
    OSCCON3 = 0x00;
    // MFOEN disabled; LFOEN disabled; ADOEN disabled; PLLEN disabled; SOSCEN disabled; EXTOEN disabled; HFOEN disabled; 
    OSCEN = 0x00;
    // HFFRQ  16_MHz; 
    OSCFRQ = 0x05;
    // TUN 0; 
    OSCTUNE = 0x00;
    // ACTUD enabled; ACTEN disabled; 
    ACTCON = 0x00;
}

void    system_initialize(void)
{
    /*
     * Function:  system_initialize
     * Used to call all initialization functions
     * for peripherals used by system
     */
    interrupt_initialize();
    _system_pmd_initialize();
    _system_oscillator_initialize();
    gpio_initialize();
    tmr2_initialize();
    tmr0_initialize();
    uart1_initialize(); 
    tmr1_initialize();
    uart2_initialize();
    tmr3_initialize();
    uart3_initialize();
    
}
