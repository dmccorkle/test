#ifndef REGISTER_H
#define	REGISTER_H

//extern modbus_register_t   g_5101;
//extern modbus_register_t   g_5102;
//extern modbus_register_t   g_5103;

extern modbus_register_t   unused_read_address;
extern modbus_register_t   unused_write_address;

// Read Register
extern modbus_register_t   g_5100;
extern modbus_register_t   g_5101;
extern modbus_register_t   g_5102;
extern modbus_register_t   g_5105;
extern modbus_register_t   g_5106;
extern modbus_register_t   g_5107;
extern modbus_register_t   g_5108;
extern modbus_register_t   g_510B;
extern modbus_register_t   g_510C;
extern modbus_register_t   g_510D;     //sensor fault Code
extern modbus_register_t   g_510F;
extern modbus_register_t   g_5110;     //sensor 1 LFL%
extern modbus_register_t   g_5111;     //sensor 2 LFL%

extern modbus_register_t   g_5115;     // \ sensor 1 serial number    
extern modbus_register_t   g_5116;     // /

extern modbus_register_t   g_5117;     // \ sensor 2 serial number
extern modbus_register_t   g_5118;     // /

extern modbus_register_t   g_51FD;     //  |
extern modbus_register_t   g_51FE;     //  ----climate master test/initialization flags 
extern modbus_register_t   g_51FF;     //  |

//Write Register
extern modbus_register_t   g_5000;     // Control Reset Value
extern modbus_register_t   g_5002;     // sensor address
extern modbus_register_t   g_5005;     // option configuration
extern modbus_register_t   g_5006;     // Alarm Threshold
extern modbus_register_t   g_5008;     // Control Flags/Status

extern modbus_register_t   g_50FD;     //  |
extern modbus_register_t   g_50FE;     //  ----climate master test/initialization flags 

void    register_load_hard_coded_values(void);

modbus_register_t *const   get_read_regs(uint16_t starting_reg);
modbus_register_t *const   get_write_regs(uint16_t starting_reg);

#endif	/* REGISTER_H */

