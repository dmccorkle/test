#include    "system/system.h"


#include    "modbus_main.h"
#include    "register.h"

#define     MY_ADDRESS g_5102.lsb_u8

modbus_state_t  modbus_main_state;
crc_t           modbus_main_crc;

void    _modbus_main_idle(void)
{
    tmr0_set_time(DELAY_20_MS);
    tmr0_reset_timer();
}

void    _modbus_main_set_receive(void)
{
    uart2_set_bytes_received(0);
    UART2_ENABLE = 0;
    uart2_set_receive_ready();
    /* TODO: COLLISION DETECTION? */
    uart2_reset_crc();
    modbus_main_state = MODBUS_RECEIVE_DONE;
    
}

void    _modbus_main_start_transmission(void)
{
    uart2_disable_interrupts();
    tmr0_clear_interrupt();
    uart2_reset_crc();
}

void    _modbus_main_set_transmit(void)
{
    modbus_main_state = MODBUS_TRANSMIT_PENDING;
    tmr0_set_time(DELAY_2_13_MS);
    tmr0_reset_timer();
}

bool    _modbus_main_check_function(void)
{
    return ((MODBUS_WRITE_REGISTER != uart2_get_input_buffer_byte(MODBUS_PACKET_COMMAND)) &&
            (MODBUS_READ_REGISTER != uart2_get_input_buffer_byte(MODBUS_PACKET_COMMAND)));
}

bool    _modbus_main_check_register_address(void)
{
    return ((0x51 != uart2_get_input_buffer_byte(MODBUS_PACKET_REGISTER_HIGH) && 
            MODBUS_READ_REGISTER == uart2_get_input_buffer_byte(MODBUS_PACKET_COMMAND)) ||
            (0x50 != uart2_get_input_buffer_byte(MODBUS_PACKET_REGISTER_HIGH) &&
            MODBUS_WRITE_REGISTER == uart2_get_input_buffer_byte(MODBUS_PACKET_COMMAND)));
}

bool    _modbus_main_check_message_length(void)
{
    return ((uart2_get_input_buffer_byte(MODBUS_PACKET_NUMBER_OF_BYTES) != (2 * uart2_get_input_buffer_byte(MODBUS_PACKET_NUMBER_OF_REGISTERS))) &&
            MODBUS_WRITE_REGISTER == uart2_get_input_buffer_byte(MODBUS_PACKET_COMMAND));
}

bool    _modbus_main_illegal_message(void)
{
    /* check for supported function, address range, data value 
     * return true if anything bad after setting message exception codes */
    uint8_t modbus_exception_code = 0x00;
    if (_modbus_main_check_function())
    {
        modbus_exception_code = 0x01;
    }
    else if (_modbus_main_check_register_address())
    {
        modbus_exception_code = 0x02;
    }
    else if (_modbus_main_check_message_length())
    {
        modbus_exception_code = 0x03;
    }
    
    if (0x00 != modbus_exception_code)
    {
       uart2_set_transmit_buffer_byte(MY_ADDRESS, MODBUS_PACKET_ADDRESS);
       uart2_set_transmit_buffer_byte(uart2_get_input_buffer_byte(MODBUS_PACKET_COMMAND) + 0x80, MODBUS_PACKET_COMMAND);
       uart2_set_transmit_buffer_byte(modbus_exception_code,2);
       uart2_set_bytes_to_transmit(3);
       return true;
    }
    return false;

}

void    _modbus_main_function_write(void)
{
    uint8_t reg_h, reg_l, num_regs, data = 0;
    uint8_t count = 0;    
    uint16_t reg;
    uint16_t temp;
    uint8_t write_pos = 7;
    modbus_register_t *temp_reg;
    
    
    if (!_modbus_main_illegal_message())
    {
        /* TODO: LOAD DATA FOR WRITE*/
        data = uart2_get_input_buffer_byte(MODBUS_PACKET_ADDRESS);
        uart2_set_transmit_buffer_byte(data, count);
        ++count;
        
        data = uart2_get_input_buffer_byte(MODBUS_PACKET_COMMAND);
        uart2_set_transmit_buffer_byte(data, count);
        ++count;
        
        reg_h = uart2_get_input_buffer_byte(MODBUS_PACKET_REGISTER_HIGH);
        uart2_set_transmit_buffer_byte(reg_h, count);
        ++count;
        
        reg_l = uart2_get_input_buffer_byte(MODBUS_PACKET_REGISTER_LOW);
        uart2_set_transmit_buffer_byte(reg_l, count);
        ++count;
                
        reg = (reg_h << 8) | reg_l;
        
        reg -= 0x5000;
        
        data = 0;
        uart2_set_transmit_buffer_byte(data, count);
        ++count;
        
        num_regs = uart2_get_input_buffer_byte(MODBUS_PACKET_NUMBER_OF_REGISTERS);
        data = num_regs;
        uart2_set_transmit_buffer_byte(data, count);
        ++count;
                       
        for(int i = 0; i < num_regs; i++){
            
            reg_h = uart2_get_input_buffer_byte(write_pos++); 
            reg_l = uart2_get_input_buffer_byte(write_pos++);
            
            temp = (reg_h << 8) | reg_l;
            
            temp_reg = get_write_regs(reg + i);
            temp_reg->value_u16 = temp;
            temp_reg = get_read_regs(reg + i);
            temp_reg->value_u16 = temp;
        }
        
        uart2_set_bytes_to_transmit(count); 
    }
}

void    _modbus_main_function_read(void)
{
    uint8_t reg_h, reg_l, num_regs, data = 0;
    uint8_t count = 0;
    uint16_t reg;
    modbus_register_t *temp_reg;
    
    
    if (!_modbus_main_illegal_message())
    {
        /* TODO: LOAD DATA FOR READ */
        data = uart2_get_input_buffer_byte(MODBUS_PACKET_ADDRESS);
        uart2_set_transmit_buffer_byte(data, count);
        ++count;
        
        data = uart2_get_input_buffer_byte(MODBUS_PACKET_COMMAND);
        uart2_set_transmit_buffer_byte(data, count);
        ++count;
        
        num_regs = uart2_get_input_buffer_byte(MODBUS_PACKET_NUMBER_OF_REGISTERS);
        data = num_regs *2;
        uart2_set_transmit_buffer_byte(data, count);
        ++count;
        
        reg_h = uart2_get_input_buffer_byte(MODBUS_PACKET_REGISTER_HIGH);
        reg_l = uart2_get_input_buffer_byte(MODBUS_PACKET_REGISTER_LOW);
        
        reg = (reg_h << 8) | reg_l;
        
        reg -= 0x5100;
        
        for(int i=0;i<num_regs;i++){
            
            temp_reg = get_read_regs(reg + i);
            if(temp_reg->value_u16 == 0){
                
                data = 0;
                uart2_set_transmit_buffer_byte(data, count);
                ++count;
                uart2_set_transmit_buffer_byte(data, count);
                ++count;
            }
            else{
                
                data = (temp_reg->value_u16 >> 8);
                uart2_set_transmit_buffer_byte(data, count);
                ++count;
                data = (temp_reg->value_u16 & 0xFF);
                uart2_set_transmit_buffer_byte(data, count);
                ++count;
            }
        }        
        
        uart2_set_bytes_to_transmit(count);        
    }
}

void    _modbus_main_command_pack(void)
{
    
    switch (uart2_get_input_buffer_byte(MODBUS_PACKET_COMMAND))
    {
        case    MODBUS_READ_REGISTER:
            _modbus_main_function_read();
            break;
        case    MODBUS_WRITE_REGISTER:
            _modbus_main_function_write();
            break;
            
    }
}

void    _modbus_main_no_state(void)
{
    if (modbus_main_crc.crc_u16 > 0)
    {
        _modbus_main_idle();
        _modbus_main_set_receive();
    }
    else if (MY_ADDRESS == uart2_get_input_buffer_byte(MODBUS_PACKET_ADDRESS))
    {
        _modbus_main_command_pack();
        _modbus_main_set_transmit();
        _modbus_main_set_receive();
        modbus_main_state = MODBUS_TRANSMIT_PENDING;
    }
    else
    {
        _modbus_main_idle();
        _modbus_main_set_receive();
    }
}

void    modbus_main_execute(void)
{
    modbus_main_crc = uart2_get_crc();
    
    switch (modbus_main_state)
    {
        case    MODBUS_COLLISION:
            _modbus_main_idle(); 
            _modbus_main_set_receive();
            break;
        case    MODBUS_IDLE:
        case    MODBUS_RECEIVE_DONE:
            _modbus_main_idle();
            _modbus_main_set_receive();
            break;
        case    MODBUS_TRANSMIT_PENDING:
            _modbus_main_start_transmission();
            uart2_send_byte();
            break;
        case    MODBUS_TRANSMIT_DONE:
            _modbus_main_idle();
            _modbus_main_set_receive();
            break;
        case    MODBUS_NO_STATE:
        default:
            _modbus_main_no_state();
            break;
    }
}

void modbus_main_set_state(modbus_state_t state)
{
    modbus_main_state = state;
}