#ifndef TIMERS_H
#define	TIMERS_H


#define     TIMER_OFF   0xFFFF

void    timers_execute(void);

bool	timers_is_tenth_second_timer_expired(uint16_t time_in, uint16_t period);
bool	timers_is_second_timer_expired(uint16_t time_in, uint16_t period);
bool	timers_is_minute_timer_expired(uint16_t time_in, uint16_t period);
bool	timers_is_hour_timer_expired(uint16_t time_in, uint16_t period);
bool	timers_is_day_timer_expired(uint16_t time_in, uint16_t period);

uint16_t	timers_now_tenth(void);
uint16_t	timers_now_second(void);
uint16_t	timers_now_minute(void);
uint16_t	timers_now_hour(void);
uint16_t	timers_now_day(void);

#endif	/* TIMERS_H */

